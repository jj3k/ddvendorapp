SHOW VARIABLES LIKE 'sql_mode';
set GLOBAL sql_mode='ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
  
-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 18) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 18), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 17) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 17), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 16) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 16), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 15) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 15), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 14) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 14), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 13) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 13), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 12) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 12), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 11) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 11), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 10) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 10), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 9) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 9), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 8) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 8), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 7) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 7), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 6) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 6), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 5) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 5), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 4) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 4), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 3) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 3), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 2) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 2), "") != ""

-- for our data, 18 is the max number of items in an order
insert into items_averages   
select getNth(o.itemIDs, 1) as item_id,
	o.id as order_id, 
	a.averagesecondsperitem as average_time
from averages a
inner join api.order o on o.id = a.id   
where ifnull(getNth(o.itemIDs, 1), "") != ""



     