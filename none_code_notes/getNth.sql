DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getNth`(idString VARCHAR(255), n INT) RETURNS varchar(255) CHARSET utf8mb4
BEGIN
IF (n = 1) THEN 
	RETURN REGEXP_SUBSTR(substring_index(idString, ",", 1), "[0-9]+"); 
ELSE
	RETURN REGEXP_SUBSTR(replace(substring_index(idString, ",", n), substring_index(idString, ",",  n-1), ""), "[0-9]+");
 END IF;
 
END$$
DELIMITER ;
