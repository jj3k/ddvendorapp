-- master file

-- 0) these orders are historical, #0 is how long it took from placed_on to pickedup_on
-- 1) average time per item -- across all ordres
-- 2) average time per item -- in batches, ie. what is the average time per item in a batch size of N items?
-- 3) attempt average time per item by averaging item time across batches 

-- #0
-- how long did the order truly take? 1454 seconds
select *
from averages
where id = 3020038

-- # 1
-- 1510.18 seconds  
select AVG(numItems * AverageSecondsPerItem)
from averages

-- #2
-- 1631.8 seconds = 5 * 326.36
select numItems, avg(averageSecondsPerItem)
from averages
group by numItems

-- # 3
select *
from items_averages
where order_id = 3020038
 
-- 1922.85 seconds
select 3020038, sum(avg_time)   
from (
	select item_id, avg(average_time) as avg_time
	from items_averages
	group by item_id
) as item_averages
where item_id in (10000025, 10000101, 10000087, 10000046, 10000073) 

-- what is the median order size 
select median(numItems) over ()
from averages 

-- what is the average order size
select avg(numItems)
from averages

-- find a random order with 3 items and do it again
select * 
from averages
where numItems = 3
limit 1
offset 458

-- run through it again
-- # 0
-- 3305943 took 1544 seconds
-- #1, 1510.18 seconds
-- #2, 3* 497.22 seconds = 1491.66 s
-- #3 1167.9 s
select 3305943, sum(avg_item_time)    
from items_averages a 
inner join item_averages_order_ind i on i.item_id = a.item_id
 where a.order_id = 3305943 and i.item_id in (10000093, 10000038, 10000046) 
 
-- let's try an order of non-standard size, say 10 items
-- I expect #1 to be wrong, #2 to be over, and #3 to be exact
select * 
from averages
where numItems = 10
limit 1
offset 7

-- # 0  -- 1670 seconds
select *
from averages
where id = 3209156

-- # 1, 1510 seconds
-- # 2, 2030 seconds
-- # 3,  1167 seconds
  
select order_id, sum(average_time) 
from items_averages
where order_id = 3209156
and item_id in (10000161, 10000084, 10000077, 10000073, 10000161, 10000097, 10000056, 10000328, 10000024, 10000088)
 
select order_id, sum(avg_item_time) as total_time 
from items_averages a 
inner join item_averages_order_ind i on i.item_id = a.item_id 
where a.order_id = 3305943  
group by order_id
 
 
 
-- looking at all orders with 10 items
select avg(foodReadySeconds), avg(vendor_duration), avg(AverageSecondsPerItem) * 10   
from averages
where numItems = 10

-- spot-check another order w 10 items
select *
from api.order
where id = 3209389

select item_id, count(order_id)
from items_averages
-- where item_id in (1405010, 1405011, 1405012, 1405013, 1405014, 1405015, 1405016, 1405017, 1405018, 1405019)
group by item_id
having count(order_id) > 1


select *
from api.order where id = 3017313

 

