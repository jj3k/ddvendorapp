package biz.deliverydudes.vendorapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.DDAppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.activity_feedback.*
import kotlinx.android.synthetic.main.feedback_toolbar.*
import okhttp3.ResponseBody

class FeedbackActivity : DDAppCompatActivity(DDAppCompatActivity.activityCode.FeedbackActivity.code) {
    private var ddOrderId = 0
    private var bFeedBackSent = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        // set the title then make the toolbar the action bar
        val toolbar_title = findViewById(R.id.toolbar) as android.support.v7.widget.Toolbar
        toolbar_title.setTitle("Feedback")
        setSupportActionBar(toolbar_title)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        val mode = intent.extras["mode"].toString()
        ddOrderId = intent.extras["OrderID"] as Int

        if (ddOrderId > 0 && mode == "order") {
            editFeedback.hint = "Information about $ddOrderId will be sent with your feedback!"
        } else {
            editFeedback.hint = resources.getString(R.string.feedback_generic_hint)
        }

        val wrappedContext = VendorContextWrapper(this)
        val apiBridge = APIBridge(wrappedContext)
        var email = ""

        Common(wrappedContext).apply {
            email = getFromSharedPrefs(Common.cache.Email.code)
        }

        val sendButtonListener: View.OnClickListener = View.OnClickListener {
            val actualFeedback = editFeedback.text.toString()

            if (!bFeedBackSent && actualFeedback.isNotBlank()) {
                bFeedBackSent = true
                val buildVersion = android.os.Build.VERSION.SDK_INT

                val submittedCBSuccess: ((response: ResponseBody) -> Unit) = {
                    Amplitude.getInstance().logEvent("Sends Feedback")
                    Toast.makeText(this, "Your feedback was sent!", Toast.LENGTH_LONG).show()

                    finish()

                    val orders = Intent(wrappedContext, Tabs::class.java)
                    startActivity(orders)
                }

                val submittedFailedCB: ((t: Throwable) -> Unit) = {
                    bFeedBackSent = false

                    Log_LogEntry(LogEntry(context = wrappedContext,
                        error_module = javaClass.name,
                        error_submodule = "submittedFailedCB",
                        message = "There was an issue submitting feedback.",
                        t = it))
                }

                apiBridge.sendFeedback(
                    type = mode,
                    message = actualFeedback,
                    nOrderID = ddOrderId,
                    app_version = BuildConfig.VERSION_NAME,
                    os_version = buildVersion.toString(),
                    successCB = submittedCBSuccess,
                    failureCB = submittedFailedCB)
            } else {
                if (bFeedBackSent)
                    Toast.makeText(this, "Your feedback was already sent.", Toast.LENGTH_SHORT).show()
                else
                    Toast.makeText(this, "You cannot send blank feedback.", Toast.LENGTH_SHORT).show()
            }
        }

        btnSend.setOnClickListener(sendButtonListener)
    }
}
