package biz.deliverydudes.vendorapp

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.ITB
import java.text.MessageFormat

class TabITB : Fragment() {
    private lateinit var ctx: Context
    private lateinit var apiBridge: APIBridge
    private lateinit var dbBridge: DBBridge
    private lateinit var listView: ListView
    lateinit var email: String
    lateinit var addedITB: ITB
    private var nITBEmailCap = 0
    private var bAdding = false
    lateinit var values: ArrayList<ITB>
    lateinit var alertDialogBuilder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ctx = VendorContextWrapper(this.activity!!.applicationContext)
        apiBridge = APIBridge(ctx)
        dbBridge = DBBridge(ctx)

        nITBEmailCap = Common(ctx).getIntFromSharedPrefs(Common.cache.InTheBizCap.code)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)

        val v: View = inflater.inflate(R.layout.fragment_itb, container, false)
        listView = v.findViewById<ListView>(R.id.listITBUsers)
        values = dbBridge.getAllITBEmailsDB()

        if (values.size != 0) {
            val image_no_itb_accounts = v.findViewById(R.id.no_itb_accounts_image) as ImageView
            val text_no_itb_accounts = v.findViewById(R.id.no_itb_accounts_text) as TextView
            image_no_itb_accounts.visibility = View.GONE
            text_no_itb_accounts.visibility = View.GONE
        }

        alertDialogBuilder = AlertDialog
                .Builder(this.activity!!, R.style.AlertDialogCustom)

        val adapter = ITBAdapter(object : ITBTrashButtonClickListener {
            override fun onButtonClicked(position: Int) {
                bAdding = false
                listener(position)
            }
        }, this.activity!!)
        adapter.data = values
        listView.adapter = adapter

        val txtWelcomeHeading = v.findViewById<TextView>(R.id.welcomeHeading)
        txtWelcomeHeading.text = Html.fromHtml(Common(ctx).loadRawResource(R.raw.in_the_biz_welcome_heading))

        // message needs {0} -> five or 15, whatever the cap is.
        val resourceWelcomeMessage = Common(ctx).loadRawResource(R.raw.in_the_biz_welcome)
        val welcomeMessage = MessageFormat.format(resourceWelcomeMessage, grammar(nITBEmailCap))
        val txtWelcomeMessage = v.findViewById<TextView>(R.id.welcomeMessage)
        txtWelcomeMessage.text = Html.fromHtml(welcomeMessage)

        val buttonAdd = v.findViewById<Button>(R.id.btnAdd)
        val emailAddressField = v.findViewById<EditText>(R.id.emailAddress)
        emailAddressField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                buttonAdd.isEnabled = Common.isValidEmail(s.toString()) || ((s ?: "").isBlank())
            }
        })

        buttonAdd.setOnClickListener {
            nITBEmailCap = Common(ctx).getIntFromSharedPrefs(Common.cache.InTheBizCap.code)
            bAdding = true
            val bFull = values.size >= nITBEmailCap
            val emailAddressToAdd = emailAddressField.text.toString().trim()

            if (emailAddressToAdd.isNotEmpty()) {
                val bAlreadyInDB = values.filter {
                    it.email_address == emailAddressToAdd
                }.isNotEmpty()

                var strToast = ""

                if (bFull)
                    strToast += "You have reached your maximum number of ${grammar(nITBEmailCap)} accounts."

                if (bAlreadyInDB)
                    strToast += (if (strToast.isNotBlank()) "\nAlso, " else "") + "$emailAddressToAdd is already a member."

                if (strToast.isNotBlank())
                    Toast.makeText(ctx, strToast, Toast.LENGTH_LONG).show()
                else {
                    // hide keyboard
                    val imm = ctx.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view!!.windowToken, 0)
                    addedITB = apiBridge.addITBInAPI(emailAddressToAdd)
                }
            } else
                Toast.makeText(ctx, "Please enter an email registered with Delivery Dudes.", Toast.LENGTH_LONG).show()
        }

        apiBridge.onAddOrDeleteITBUser = {
            // userHandle = "" in all cases but successfully added.
            bSuccess, userHandle ->
            this.activity!!.runOnUiThread {
                // refresh the view
                values = dbBridge.getAllITBEmailsDB()

                if (bSuccess) {
                    adapter.notifyDataSetInvalidated()
                    adapter.notifyDataSetChanged()
                    listView.invalidateViews()
                    listView.refreshDrawableState()

                    adapter.data = values
                    listView.adapter = adapter

                    val textIslandWUmbrella = v.findViewById(R.id.no_itb_accounts_image) as ImageView
                    val textIslandWUmbrellaText = v.findViewById(R.id.no_itb_accounts_text) as TextView

                    // clear added value
                    if (bAdding)
                        emailAddressField.setText("")

                    textIslandWUmbrella.visibility = if (values.size == 0) View.VISIBLE else View.GONE
                    textIslandWUmbrellaText.visibility = if (values.size == 0) View.VISIBLE else View.GONE

                    if (bAdding)
                        Toast.makeText(ctx, "Congrats! $userHandle is now a member of the Dudes' in the business program.", Toast.LENGTH_LONG).show()
                    else
                        Toast.makeText(ctx, "$userHandle was successfully removed.", Toast.LENGTH_LONG).show()
                } else {
                    if (bAdding)
                        Toast.makeText(ctx, "Email must be tied to a registered dude account.", Toast.LENGTH_LONG).show()
                    else
                        Toast.makeText(ctx, "An unknown error occurred deleting the user.", Toast.LENGTH_LONG).show()
                }
            }
        }

        listView.setOnItemClickListener { parent, view, position, id ->
            listener(position)
        }

        return v
    }

    private fun grammar(inbound: Int) = when (inbound) {
        // grammar rule is if ten or under, it's spelled out. Over ten is numerical.
        1 -> "one"
        2 -> "two"
        3 -> "three"
        4 -> "four"
        5 -> "five"
        6 -> "six"
        7 -> "seven"
        8 -> "eight"
        9 -> "nine"
        10 -> "ten"
        else -> inbound.toString()
    }

    private fun listener(position: Int) {
        val itb = values[position]
        val selectedEmail = itb.email_address
        val fname = itb.first_name
        val lname = itb.last_name

        val alertDialog = alertDialogBuilder.create()

        // are you sure? y/n
        alertDialog.let {
            it.setCancelable(true)
            it.setTitle("Remove ITB Account")
            it.setMessage("$fname $lname will no longer get free deliveries. Are you sure?")
            it.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", DialogInterface.OnClickListener({ dialog, which ->
                dialog.dismiss()
            }))
            it.setButton(AlertDialog.BUTTON_POSITIVE, "Delete", DialogInterface.OnClickListener({ dialog, which ->
                apiBridge.deleteITBInAPI(itb.id, selectedEmail)
            }))
        }
        alertDialog.window.setLayout(600, 200)
        alertDialog.show()
    }
}
