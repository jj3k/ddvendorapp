package biz.deliverydudes.vendorapp.recyclerViews

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import biz.deliverydudes.vendorapp.helpers.Network.APIService
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.LogEntry

class WrapLLM(val ctx: Context) : LinearLayoutManager(ctx) {
    // I only saw it crash ONE time when I marked 'pickedup' to about 15 orders. -- JJ.
    // Of course, then I tried it with 25 orders and it didn't crash. 👽
    // https://stackoverflow.com/questions/31759171/recyclerview-and-java-lang-indexoutofboundsexception-inconsistency-detected-in
    override fun onLayoutChildren(recycler: RecyclerView.Recycler?, state: RecyclerView.State) {
        try {
            super.onLayoutChildren(recycler, state)
        } catch (ex: IndexOutOfBoundsException) {
            Log_LogEntry(LogEntry(context = ctx,
                error_module = javaClass.name,
                error_submodule = "OrderActivity.onCreate",
                http_request_base_url = APIService.API_URL,
                message = "A recycler view item was modified in another thread; the UI thread didn't like that (IndexOutOfBoundsException).",
                sourceClass = javaClass.name,
                sourceMethod = "OrderActivity.onCreate",
                t = ex))
        }
    }
}