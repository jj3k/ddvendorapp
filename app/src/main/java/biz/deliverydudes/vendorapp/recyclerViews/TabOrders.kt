package biz.deliverydudes.vendorapp.recyclerViews

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.Network.PusherClient
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.amplitude.api.Amplitude

class TabOrders : Fragment() {
    lateinit var rv: RecyclerView
    lateinit var refreshOrders: SwipeRefreshLayout
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    lateinit var ctx: Context
    lateinit var apiBridge: APIBridge
    lateinit var dbBridge: DBBridge
    private var broadcastReceiver: BroadcastReceiver? = null

    companion object {
        var mpOrder: MediaPlayer? = null
        var mpCancelled: MediaPlayer? = null
        var mpPicked: MediaPlayer? = null

        var pollOrdersHandler: android.os.Handler = Handler()
        var runEvery12hHandler: android.os.Handler = Handler()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ctx = VendorContextWrapper(this.activity!!.applicationContext)
        apiBridge = APIBridge(ctx)
        dbBridge = DBBridge(ctx)

        // Pusher (native) and Pusher (web sockets)
        val pusher = PusherClient(this.activity!!, { onOrdersRefreshed() })
        pusher.subscribe()

        // ... and again every 180s (3 mins)
        val nPollingIntervalSeconds = resources.getInteger(R.integer.polling_interval_seconds)
        var pollOrdersTask: Runnable? = null

        pollOrdersTask = Runnable {
            apiBridge.getAPIOrders(0, { onOrdersRefreshed() })
            pollOrdersHandler.postDelayed(pollOrdersTask, nPollingIntervalSeconds.toLong() * 1000)
        }

        pollOrdersHandler.post(pollOrdersTask)

        // automatically 'picks' orders that are over 12 hours old, every 12 hours.
        var runEvery12h: Runnable? = null
        runEvery12h = Runnable {
            pickOrdersOver12HoursOld()

            runEvery12hHandler.postDelayed(runEvery12h, 12 * 60 * 60 * 1000)
        }

        runEvery12hHandler.post(runEvery12h)
    }

    private fun onOrdersRefreshed() {
        activity?.runOnUiThread {
            try {
                val ordersShowing = dbBridge.getOpenOrders()

                (rv.adapter as DataAdapterOrders).dataset = ordersShowing

                val unassignedOrders = ordersShowing.filter {
                    it.status == Order.Status.Placed.code && it.foodReady == 0
                }

                // if they're in the order activity and there's a new order, shhh.
                if (unassignedOrders.count() > 0) {
                    Common(ctx).soundNotification()
                } else {
                    Common(ctx).stfu()
                }

                // if they're looking at a current order and it changes
                // in the cloud, the pusher/poller code comes here.
                // Close the OrderActivity, go back to queue.
                if (OrderActivity.bIsInView && OrderActivity.mode == Order.Mode.Current) {
                    val orderInView = dbBridge.getOrder(OrderActivity.ddOrderId)
                    val bInvalidated = !arrayOf(Order.Status.Placed, Order.Status.Assigned).contains(Order.Status.from(orderInView.status))

                    if (bInvalidated) {
                        val toastText = resources.getString(R.string.order_status_changed)
                        Toast.makeText(ctx, toastText, Toast.LENGTH_LONG).show()

                        val exitOrder = Intent("finish_activity")
                        this.activity?.sendBroadcast(exitOrder)
                    }
                }

                rv.adapter.notifyDataSetChanged()
                refreshOrders.isRefreshing = false
            } catch (ex: TypeCastException) {
                // somehow they refreshed before the init of the rv.adapter:
                // "Fatal Exception: kotlin.TypeCastException: null cannot be cast to non-null type biz.deliverydudes.vendorapp.recyclerViews.DataAdapterOrders"
                // don't do anything.
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_orders, container, false)
        rv = v.findViewById<RecyclerView>(R.id.orders_recycler)
        refreshOrders = v.findViewById<SwipeRefreshLayout>(R.id.refreshOrders)

        viewManager = WrapLLM(ctx)
        viewAdapter = DataAdapterOrders(this.activity!!, object : OrdersRecyclerClickListener {
            override fun onLongClicked(position: Int) {}

            // green assign time button
            override fun onPositionClicked(position: Int) {
                loadOrder(position)
            }
        }, object : OrdersRecyclerButtonClickListener {
            // on click of the Pseudo button
            override fun onButtonClicked(position: Int) {
                loadOrder(position)
            }
        })

        rv.apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
            addItemDecoration(DividerItemDecoration(ctx, DividerItemDecoration.VERTICAL))
        }

        refreshOrders.setOnRefreshListener {
            Amplitude.getInstance().logEvent("Swipes to Refresh")

            // we want the lastOrdersRetrievedAt date to be as far in the past as possible
            // Common(ctx).addToSharedPrefs(Common.cache.LastOrdersRetrievedAt.code, DateTime().toString())
            apiBridge.getAPIOrders(0, { onOrdersRefreshed() })
        }

        return v
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        onOrdersRefreshed()
    }

    override fun onStart() {
        super.onStart()

        // when time is added in the AddTimeDialog, a broadcast receiver comes here
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(arg0: Context, intent: Intent) {
                val action = intent.action
                if (action == "refresh_orders") {
                    onOrdersRefreshed()
                }
            }
        }

        this.activity!!.registerReceiver(broadcastReceiver, IntentFilter("refresh_orders"))
    }

    override fun onStop() {
        super.onStop()
        this.activity!!.unregisterReceiver(broadcastReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()

        rv.adapter = null

        val wrappedContext = VendorContextWrapper(this.activity!!.applicationContext)
        val apiBridge = APIBridge(wrappedContext)
        apiBridge.logout()
    }

    fun loadOrder(position: Int) {
        // stop the sound
        // Common(ctx).stfu()

        val indivOrder = Intent(context, OrderActivity::class.java)
        indivOrder.putExtra("position", position)
        indivOrder.putExtra("mode", Order.Mode.Current.code)

        startActivityForResult(indivOrder, 0)
    }

    private fun pickOrdersOver12HoursOld() {
        val bOrdersOver12HoursOldPicked = dbBridge.autoPickOrders12HrsOld()

        if (BuildConfig.DEBUG && !bOrdersOver12HoursOldPicked)
            Log_LogEntry(LogEntry(context = ctx,
                    error_module = javaClass.name,
                    error_submodule = "onCreate",
                    message = "An unknown error was encountered autopicking placed/assigned orders 12+ hours old.",
                    t = Throwable("An unknown error was encountered autopicking placed/assigned orders 12+ hours old.")))

        onOrdersRefreshed()
    }
}