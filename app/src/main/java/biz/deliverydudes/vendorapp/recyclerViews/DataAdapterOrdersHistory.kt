package biz.deliverydudes.vendorapp.recyclerViews

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import kotlinx.android.synthetic.main.orders_history_row.view.*
import org.joda.time.DateTime

class DataAdapterOrdersHistory(
    val activity: Activity,
    val rowListener: OrdersRecyclerClickListener
)
    : RecyclerView.Adapter<DataAdapterOrdersHistory.ViewHolder>() {
    private val wrappedContext = VendorContextWrapper(activity.applicationContext)
    private val apiBridge = APIBridge(wrappedContext)
    private val dbBridge = DBBridge(wrappedContext)
    var dataset = dbBridge.getHistoricalOrders("DESC")

    companion object {
        var mClickListener: OrdersRecyclerClickListener? = null
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than TabOrders view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val rowView: RelativeLayout) : RecyclerView.ViewHolder(rowView)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DataAdapterOrdersHistory.ViewHolder {

        // create a new view
        val row = LayoutInflater.from(parent.context)
                .inflate(R.layout.orders_history_row, parent, false) as RelativeLayout

        return ViewHolder(row)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            val nAdapterPosition = holder.adapterPosition
            val historyOrder = dataset[nAdapterPosition]

            holder.setIsRecyclable(false)
            mClickListener = rowListener

            val textViewOrderNumber = holder.rowView.textViewHOrderNumber
            val textViewCookTime = holder.rowView.textViewHCookTime
            val textViewReceivedAt = holder.rowView.textViewHReceivedAt
            val textViewSales = holder.rowView.textViewHSales

            val strFoodReady = historyOrder.foodReady.toString()
            val placedOn = DateTime.parse(historyOrder.placed_on)

            textViewOrderNumber.text = historyOrder.id.toString()
            textViewCookTime.text = strFoodReady

            var receivedAt = ""
            var salesText = ""

            try {
                receivedAt = placedOn.toString("M/dd/YY h:mm a")
                salesText = "$${ "%.2f".format(historyOrder.total.toDouble() / 100.0) }"
            } catch (ex: Exception) {
                Log_LogEntry(LogEntry(context = wrappedContext,
                    error_module = javaClass.name,
                    error_submodule = "onBindViewHolder",
                    message = "Can't cast ${historyOrder.total} to double, or can't cast {${historyOrder.placed_on} to 'M/dd/YY h:mm a' string : ${ex.message}.",
                    t = ex))
            }

            textViewReceivedAt.text = receivedAt
            textViewSales.text = salesText

            holder.rowView.setOnClickListener {
                if (mClickListener != null) {
                    mClickListener?.onPositionClicked(nAdapterPosition)
                }
            }
        } catch (ex: IndexOutOfBoundsException) {
            Log_LogEntry(LogEntry(context = wrappedContext,
                error_message = ex.message.toString(),
                error_module = javaClass.name,
                error_submodule = "onBindViewHolder",
                message = "There was an unexpected IndexOutOfBoundsException: ${ex.message}.",
                t = ex))
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = wrappedContext,
                error_message = ex.message.toString(),
                error_module = javaClass.name,
                error_submodule = "onBindViewHolder",
                message = "There was an unexpected exception: ${ex.message}.",
                t = ex))
        }
    }

    override fun getItemCount(): Int {
        return dbBridge.getOrderHistoryCount()
    }
}
