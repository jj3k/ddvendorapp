package biz.deliverydudes.vendorapp.recyclerViews

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.AddTimeDialog
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.DDAppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Network.APIService
import biz.deliverydudes.vendorapp.helpers.Network.Firebase
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.order_row_footer.*
import kotlinx.android.synthetic.main.toolbar.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

// OrderActivity handles current and historical orders.
// ... handles orders that need time assigned as well as
// orders that already have time assigned.
// If they come to this screen, but do NOT
// assign time in 90 s, the sound will play again and they
// will be sent back to TabOrders to assign time.
class OrderActivity : DDAppCompatActivity(DDAppCompatActivity.activityCode.OrderActivity.code) {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapterAssigned: RecyclerView.Adapter<*>
    private lateinit var viewAdapterUnassigned: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private val handler = android.os.Handler()
    private val wrappedContext = VendorContextWrapper(this)
    var bHasBeenAssignedTime = false
    var alertRunnable: Runnable? = null
    lateinit var refreshOrder: SwipeRefreshLayout
    private var broadcastReceiver: BroadcastReceiver? = null

    companion object globalOrderActivity {
        var bIsInView = false
        var ddOrderId = 0
        var mode = Order.Mode.Current
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val apiBridge = APIBridge(wrappedContext)
        val dbBridge = DBBridge(wrappedContext)

        setContentView(R.layout.activity_order)

        // mode is history or current order
        mode = Order.Mode.from(intent.getStringExtra("mode"))
        val orderBy = intent.getStringExtra("orderBy")
        // order number is inbound from prev activity
        val appOrderId: Int = intent.getIntExtra("position", 0)
        // an offset, not a real orderID
        val order = if (mode == Order.Mode.Current) dbBridge.getOrderAt(appOrderId) else dbBridge.getHistoryOrderAt(appOrderId, orderBy)
        val toolbarTitle = if (mode == Order.Mode.Current) "Queue" else "History"
        ddOrderId = order.id
        var orderStatus = Order.Status.Placed

        Amplitude.getInstance().logEvent("Selects the Order", Common.JSONForAmplitude(ddOrderId))

        // stop all sounds, unless this is an historical order
        // if (mode == Order.Status.Placed)
        //    Common(wrappedContext).stfu()

        try {
            orderStatus = Order.Status.from(order.status)
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = wrappedContext,
                    error_message = ex.message.toString(),
                    error_module = javaClass.name,
                    error_submodule = "OrderActivity.onCreate",
                    http_request_base_url = APIService.API_URL,
                    sourceClass = javaClass.name,
                    sourceMethod = "OrderActivity.onCreate",
                    message = "The application sought status of ${order.status} in the Status enum, and did not find it: ${ex.message}",
                    t = ex))
        }

        bHasBeenAssignedTime = ((orderStatus == Order.Status.Placed || orderStatus == Order.Status.Assigned) && order.foodReady > 0)

        refreshOrder = findViewById<SwipeRefreshLayout>(R.id.refreshOrder)
        refreshOrder.isEnabled = mode == Order.Mode.Current
        val tb = findViewById<TextView>(R.id.toolbar_text)

        try {
            val subTotal = "%.2f".format(order.total.toDouble() / 100.0)

            tb.text = "Order ${order.id} - $$subTotal"
            tb.textSize = 30f
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = wrappedContext,
                    error_message = ex.message.toString(),
                    error_module = javaClass.name,
                    error_submodule = "OrderActivity.onCreate",
                    http_request_base_url = APIService.API_URL,
                    sourceClass = javaClass.name,
                    sourceMethod = "OrderActivity.onCreate",
                    message = "Order #$ddOrderId had no subtotal (may be empty order).",
                    t = ex))
        }

        val nNumRows = order.items.count()

        if (nNumRows == 0) {
            // zero items not allowed
            Log_LogEntry(LogEntry(context = wrappedContext,
                    error_message = "Order #$ddOrderId had zero items.",
                    error_module = javaClass.name,
                    error_submodule = "OrderActivity.onCreate",
                    http_request_base_url = APIService.API_URL,
                    sourceClass = javaClass.name,
                    sourceMethod = "OrderActivity.onCreate",
                    message = "Order #$ddOrderId had zero items.",
                    t = Throwable("Order #$ddOrderId had zero items.")))
        }

        // set the title then make the toolbar the action bar
        val toolbar_title = findViewById(R.id.toolbar) as android.support.v7.widget.Toolbar
        toolbar_title.setTitle(toolbarTitle)
        setSupportActionBar(toolbar_title)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        // / TODO create an android shape (v, inverted v, or stripe pattern) for the scrollbar
        viewManager = LinearLayoutManager(this)

        // no time has been assigned
        viewAdapterUnassigned = DataAdapterOrder(this, appOrderId, mode, object : DataAdapterOrder.BtnClickListener {
            override fun onBtnClick(position: Int) {
                Amplitude.getInstance().logEvent("Assigns Time")

                btnAssignTimeToOrder.isEnabled = false

                // they clicked 'assign time' -- food_ready was 0, will now be > 0
                val progressBar = findViewById<SeekBar>(R.id.seekBar)
                val numberOfMinutes = progressBar.progress

                Firebase(this@OrderActivity, order.id).apply {
                    if (bHasBeenAssignedTime) assign_time() else view_order()
                }

                // update the order with timestamp = now(utc) + assigned time
                val readyAt = DateTime(DateTimeZone.UTC).plusMinutes(numberOfMinutes)
                apiBridge.setReadyAt(order.id, readyAt, false)

                apiBridge.onOrderUpdated = {
                    order.foodReady = it.foodReady
                    order.placed_on = it.placed_on
                    order.modified_on = it.modified_on
                    // this is for the trigger. It gets overwritten by the cloud.
                    order.status = Order.Status.Assigned.code

                    // if (BuildConfig.DEBUG) Log.i("runPs", "it.placed_on.toString()::" + it.placed_on.toString())

                    dbBridge.updateOrderDB(order)
                    if (BuildConfig.DEBUG) Log.i(javaClass.name, "User assigned $numberOfMinutes minutes to order ${order.id}")

                    val resultIntent = Intent()
                    resultIntent.putExtra("position", appOrderId)
                    resultIntent.putExtra("AssignedTimeInMinutes", numberOfMinutes)

                    setResult(Activity.RESULT_OK, resultIntent)
                    finish()
                }

                apiBridge.onOrderUpdateFailed = {
                    btnAssignTimeToOrder.isEnabled = true
                    AddTimeDialog().onOrderUpdateFailed(this@OrderActivity, it)
                }
            }
        })

        // time has been assigned
        viewAdapterAssigned = DataAdapterOrder(this, appOrderId, mode, object : DataAdapterOrder.BtnClickListener {
            override fun onBtnClick(position: Int) {
                Amplitude.getInstance().logEvent("Taps Ready for Pickup")

                // they clicked 'ready for pickup' -- food_ready > 0
                Firebase(this@OrderActivity, order.id).apply {
                    if (bHasBeenAssignedTime) assign_time() else view_order()
                }

                // update the order with timestamp = now(utc) + assigned time
                val readyAt = DateTime(DateTimeZone.UTC)
                apiBridge.setReadyAt(order.id, readyAt, true)

                apiBridge.onOrderUpdated = {
                    order.foodReady = it.foodReady
                    order.placed_on = it.placed_on
                    order.pickedup_on = it.pickedup_on
                    order.modified_on = it.modified_on
                    // this is for the trigger. It gets overwritten by the cloud.
                    order.status = "RFP"

                    dbBridge.updateOrderDB(order)

                    val resultIntent = Intent()
                    resultIntent.putExtra("position", appOrderId)
                    resultIntent.putExtra("AssignedTimeInMinutes", 0)

                    setResult(Activity.RESULT_OK, resultIntent)
                    finish()
                }

                apiBridge.onOrderUpdateFailed = {
                    AddTimeDialog().onOrderUpdateFailed(this@OrderActivity, it)
                }
            }
        })

        recyclerView = findViewById<RecyclerView>(R.id.order_recycler).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            isNestedScrollingEnabled = false

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = if (order.foodReady == 0) viewAdapterUnassigned else viewAdapterAssigned

            if (mode == Order.Mode.Current) addItemDecoration(StickyFooterItemDecoration())
            addItemDecoration(DividerItemDecoration(wrappedContext, DividerItemDecoration.VERTICAL))
        }

        refreshOrder.setOnRefreshListener {
            Amplitude.getInstance().logEvent("Swipes to Refresh")

            apiBridge.getAPIOrder(order.id, {
                if (it.status != Order.Status.Placed.code &&
                        it.status != Order.Status.Assigned.code) {
                    val toastText = resources.getString(R.string.order_status_changed)
                    Toast.makeText(wrappedContext, toastText, Toast.LENGTH_LONG).show()
                    finish()
                }
            })
            refreshOrder.isRefreshing = false
        }

        // button is gone by default since the toolbar is reused
        btnChangeTime.visibility = if (mode == Order.Mode.History) View.GONE else if (bHasBeenAssignedTime) View.VISIBLE else View.INVISIBLE

        btnChangeTime.setOnClickListener {
            AddTimeDialog().showDialog(this, appOrderId, order)
        }

        // so if they don't assign time after 90 seconds,
        // sound again; go back to orders queue.
        alertRunnable = get90sRunnable(mode, order)
        val nNumberOfSeconds = resources.getInteger(R.integer.interaction_before_sound_seconds)
        handler.postDelayed(alertRunnable, nNumberOfSeconds.toLong() * 1000)

        // this broadcast receiver is called from TabOrders.kt,
        // which receives the pusher callback. So the OrderActivity is
        // closed when there is a status change.
        broadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(arg0: Context, intent: Intent) {
                val action = intent.action
                if (action == "finish_activity") {
                    finish()
                }
            }
        }
        registerReceiver(broadcastReceiver, IntentFilter("finish_activity"))
    }

    private fun get90sRunnable(mode: Order.Mode, order: Order): Runnable {
        return Runnable {
            // Log.i(javaClass.name, "90 second timer's current dateTime(): ${DateTime().toString()}")

            val orderStatus = Order.Status.from(order.status)

            bHasBeenAssignedTime = ((orderStatus == Order.Status.Placed || orderStatus == Order.Status.Assigned) && order.foodReady > 0)
            if (mode == Order.Mode.Current && !bHasBeenAssignedTime) {
                val resultIntent = Intent()
                setResult(Activity.RESULT_OK, resultIntent)
                finish()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        bIsInView = true
    }

    override fun onStop() {
        super.onStop()
        alertRunnable = Runnable { }
        bIsInView = false

        try {
            unregisterReceiver(broadcastReceiver)
        } catch (ex: Exception) {
            // do nothing
        }
    }

    override fun onPause() {
        super.onPause()
        bIsInView = false
    }
}
