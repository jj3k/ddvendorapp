package biz.deliverydudes.vendorapp.recyclerViews

import android.app.Activity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.SeekBar
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.CartItem
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.order_row.view.*
import kotlinx.android.synthetic.main.order_row_footer.view.*
import org.joda.time.DateTime
import kotlin.math.roundToInt

class DataAdapterOrder(
    val activity: Activity,
    appOrderID: Int,
    private val orderMode: Order.Mode,
    private val btnListener: BtnClickListener
) : RecyclerView.Adapter<DataAdapterOrder.ViewHolder>() {
    private val wrappedContext = VendorContextWrapper(activity.applicationContext)
    private val apiBridge = APIBridge(wrappedContext)
    private val dbBridge = DBBridge(wrappedContext)

    companion object {
        var mClickListener: BtnClickListener? = null
    }

    var orderData: Order
    var orderStatus: Order.Status
    var foodReady: Int = 0

    init {
        orderData = if (orderMode == Order.Mode.Current) dbBridge.getOrderAt(appOrderID) else dbBridge.getHistoryOrderAt(appOrderID, "DESC")
        orderStatus = Order.Status.from(orderData.status)
        foodReady = orderData.foodReady
    }

    // assigned time
    enum class RowType(val code: Int) {
        Data(0),
        UnassignedLast(1),
        AssignedLast(2);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than TabOrders view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val rowView: RelativeLayout) : RecyclerView.ViewHolder(rowView)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DataAdapterOrder.ViewHolder {
        // ordinary row 0
        // last row unassigned 1
        // last row assigned 2
        val layoutInflater = LayoutInflater.from(parent.context)
        val row = when (viewType) {
            RowType.UnassignedLast.code ->
                when (orderMode) {
                    Order.Mode.Current -> layoutInflater.inflate(R.layout.order_row_footer, parent, false)
                    else -> layoutInflater.inflate(R.layout.spare_row, parent, false)
                }
            RowType.AssignedLast.code ->
                layoutInflater.inflate(R.layout.assigned_order_footer, parent, false)
            else -> // Data
                layoutInflater.inflate(R.layout.order_row, parent, false)
        } as RelativeLayout

        return ViewHolder(row)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mClickListener = btnListener
        var cname: String? = null
        var pname: String? = null
        var addl: String? = null
        var pPrice: Double? = null
        var qty: Int? = null
        val cartData = orderData.items.getOrNull(position)

        // if cart is empty, notify devs
        if (orderData.items.count() == 0)
            Log_LogEntry(LogEntry(context = wrappedContext,
                error_message = "This adapter received an order with no cart items: ${orderData.id}",
                error_module = javaClass.name,
                error_submodule = "onBindViewHolder"))

        // the structure of this recycler view is a little different.
        // the data are row type 0, the footer is row type 1, with the seekbar (slider)
        when (getItemViewType(position)) {
            RowType.Data.code -> {
                cartData?.let {
                    cname = it.categoryName
                    pname = splitLines(it.productName ?: "")
                    addl = it.additionalInstructions?.trim()
                    pPrice = it.productPrice?.toDouble()
                    qty = it.quantity
                }

                holder.rowView.apply {
                    textViewPrice.text = "$%.2f".format(pPrice)

                    textAddlInstructions.visibility = if (addl.isNullOrEmpty()) View.GONE else View.VISIBLE
                    textAddlInstructions.text = addl

                    textQuantity.text = Html.fromHtml("<b>$qty</b>x")
                    textItemDescription.text = pname
                    textItemCategory.text = cname
                }
            }
            RowType.UnassignedLast.code -> {
                // if this is a historical order, the 'spare row' is showing (empty)
                if (orderMode == Order.Mode.Current) {
                    val seekBar = holder.rowView.seekBar
                    val progressView = holder.rowView.textViewSeekbar
                    val minutesView = holder.rowView.textViewNumMinutes
                    val btnAssignTimeToOrder = holder.rowView.btnAssignTimeToOrder

                    val nDefaultPrepTimeInMins = holder.rowView.resources.getInteger(R.integer.default_prep_time_in_minutes)
                    val nSuggestedPrepTimeInMins = dbBridge.getSuggestionInMins(orderData.count())
                    val bSuggestTime = nSuggestedPrepTimeInMins > -5

                    if (bSuggestTime) {
                        Amplitude.getInstance().logEvent("Has Suggestion", Common.Companion.JSONForAmplitude(hashMapOf("minutes" to nSuggestedPrepTimeInMins)))
                    }

                    seekBar.apply {
                        setMax(60)
                        setProgress(0)

                        setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                            override fun onProgressChanged(
                                seekBar: SeekBar,
                                progress: Int,
                                fromUser: Boolean
                            ) {
                                val realProgress = (progress / 5) * 5
                                seekBar.progress = realProgress

                                if (progress == realProgress) {
                                    progressView!!.bringToFront()

                                    btnAssignTimeToOrder.isEnabled = (realProgress > 0)
                                    btnAssignTimeToOrder.setBackgroundColor(ContextCompat.getColor(context,
                                        if (realProgress > 0) R.color.primary else R.color.ddDisabledButton))

                                    val max = seekBar.getMax()
                                    val offset = seekBar.getThumbOffset()
                                    val percent = progress.toFloat() / max.toFloat()

                                    // this is because on first load they return 0 because they havn't rendered yet
                                    val seekbarWidth = if (seekBar.width == 0) 972 else seekBar.width
                                    val minutesViewWidth = if (minutesView.width == 0) 1253 else minutesView.width
                                    val width = seekbarWidth - (2 * offset)

                                    var answer = (width * percent + offset - minutesViewWidth / 2).roundToInt()

                                    // this prevents 5 minutes from being cut off by the left border
                                    if (answer < -400) { answer = -400 }
                                    // this prevents 60 minutes from being cut off by the button on the right
                                    if (answer > 246) { answer = 246 }

                                    minutesView.setX(answer.toFloat())
                                    minutesView.text = "$realProgress minutes"
                                }
                            }

                            override fun onStartTrackingTouch(seekBar: SeekBar) { }

                            override fun onStopTrackingTouch(seekBar: SeekBar) {
                                val realProgress = seekBar.progress
                                Amplitude.getInstance().logEvent("Slides the slider", Common.Companion.JSONForAmplitude(hashMapOf("minutes" to realProgress)))
                            }
                        })

                        // has to come after the listener is set
                        incrementProgressBy(if (bSuggestTime) nSuggestedPrepTimeInMins else nDefaultPrepTimeInMins)
                    }

                    btnAssignTimeToOrder.setOnClickListener {
                        mClickListener?.onBtnClick(position)
                    }
                } else {
                    // history
                }
            }
            else -> {
                val assignedTimeInMillis = orderData.foodReady * 60000
                val placedOn = DateTime.parse(orderData.placed_on)
                val readyAt = placedOn.plusMillis(assignedTimeInMillis)
                val timeRemaining = readyAt.millis - System.currentTimeMillis()

                // if there is no time remaining, ready for pickup button should not show, as it
                // is already "ready for pickup" in the queue.
                val btnReadyForPickup = holder.rowView.findViewById<Button>(R.id.btnReadyForPickup)
                btnReadyForPickup.visibility = if (timeRemaining >= 0) View.VISIBLE else View.INVISIBLE

                btnReadyForPickup.setOnClickListener {
                    mClickListener?.onBtnClick(position)
                }
            }
        }

        // user is clicking on cart item -- nothing to do though
        /* holder.rowView.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View) {
                if (mClickListener != null) {
                    mClickListener?.onBtnClick(position)
                }
            }
        }) */
    }

    interface BtnClickListener {
        fun onBtnClick(position: Int)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = ((orderData.items as? ArrayList<CartItem>)?.count() ?: 0) +
        (if (orderMode == Order.Mode.Current) 1 else 0)

    override fun getItemViewType(position: Int): Int {
        // ordinary row 0
        // last row unassigned 1
        // last row assigned 2
        val bIsAssignedTime = ((orderStatus == Order.Status.Placed || orderStatus == Order.Status.Assigned) && foodReady > 0)

        val currentOrderRowType = if (position == itemCount - 1)
                                            if (bIsAssignedTime) RowType.AssignedLast.code
                                            else RowType.UnassignedLast.code
                                        else RowType.Data.code

        val historyOrderRowType = RowType.Data.code

        return if (orderMode == Order.Mode.Current) currentOrderRowType else historyOrderRowType
    }

    private fun splitLines(original: String): String {
        // this is to prevent having to change the layout type right now.
        // line length is 35

        try {
            // break recursion
            if (original.length <= 35) return original

            var nBreak = 0

            // find the space before the 35th character to break
            for (i in 34.downTo(0)) {
                if (original[i] == " ".toCharArray()[0]) {
                    nBreak = i
                    break
                }
            }

            return original.substring(0, nBreak) + "\n" + splitLines(original.substring(nBreak + 1, original.length))
        } catch (ex: Exception) {
            return original
        }
    }
}
