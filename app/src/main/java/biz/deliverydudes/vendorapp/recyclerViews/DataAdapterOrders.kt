package biz.deliverydudes.vendorapp.recyclerViews

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.AddTimeDialog
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.CartItem
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.activity_progress_button.view.*
import kotlinx.android.synthetic.main.orders_row.view.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import kotlin.math.roundToInt

enum class VIEW_TYPE(val code: Int) {
    SPARE(-1),
    EMPTY(0),
    NOT_EMPTY(1);

    companion object {
        fun from(getCode: Int): VIEW_TYPE = VIEW_TYPE.values().first { it.code == getCode }
    }
}

class DataAdapterOrders(
    val activity: Activity,
    private val rowListener: OrdersRecyclerClickListener,
    private val buttonListener: OrdersRecyclerButtonClickListener
)
    : RecyclerView.Adapter<DataAdapterOrders.ViewHolder>() {
    private val wrappedContext = VendorContextWrapper(activity.applicationContext)
    private val apiBridge = APIBridge(wrappedContext)
    private val dbBridge = DBBridge(wrappedContext)
    var dataset: ArrayList<Order> = dbBridge.getOpenOrders()

    companion object {
        var mClickListener: OrdersRecyclerClickListener? = null
        var mButtonListener: OrdersRecyclerButtonClickListener? = null
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than TabOrders view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(val rowView: RelativeLayout) : RecyclerView.ViewHolder(rowView)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DataAdapterOrders.ViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val row = when (viewType) {
            VIEW_TYPE.NOT_EMPTY.code ->
                layoutInflater.inflate(R.layout.orders_row, parent, false)
            VIEW_TYPE.SPARE.code ->
                layoutInflater.inflate(R.layout.spare_row, parent, false)
            else ->
                layoutInflater.inflate(R.layout.no_orders_row, parent, false)
        } as RelativeLayout

        return ViewHolder(row)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val nAdapterPosition = holder.adapterPosition

        when (getItemViewType(nAdapterPosition)) {
            VIEW_TYPE.EMPTY.code -> {
            }
            VIEW_TYPE.NOT_EMPTY.code -> {
                var order: Order = Order(0, 0, "", ArrayList<CartItem>(), "", "", "", 0.0, "")

                try {
                    order = dataset[nAdapterPosition]
                } catch (ex: Exception) {
                    Log_LogEntry(LogEntry(context = wrappedContext,
                            error_module = javaClass.name,
                            error_submodule = "onBindViewHolder",
                            message = "Index $nAdapterPosition was not found in the dataset (size ${dataset.size}). (Seen before as a result " +
                                    "of a sudden change in status to many orders.) Ex message: ${ex.message}",
                            t = ex
                    ))
                }

                holder.setIsRecyclable(false)
                mClickListener = rowListener
                mButtonListener = buttonListener

                val textViewOrderNumber = holder.rowView.textViewOrderNumber
                val textViewOrderStatus = holder.rowView.textViewOrderStatus
                val btnAssignTime = holder.rowView.btnAssignTime
                val imgPreparing = holder.rowView.imgPreparing
                val imgOMW = holder.rowView.imgOMW
                val bRFP = dbBridge.isRFP(order)

                textViewOrderNumber.text = order.id.toString()
                textViewOrderStatus.text = when (order.status) {
                    Order.Status.Placed.code, Order.Status.Assigned.code -> "Preparing"
                    // there should be no else
                    else -> order.status
                }

                imgPreparing.visibility = if ((order.status == Order.Status.Placed.code ||
                                order.status == Order.Status.Assigned.code) && !bRFP) View.VISIBLE else View.GONE

                imgOMW.visibility = if (bRFP) View.VISIBLE else View.GONE

                // Submitted = 0, Confirmed = 1, Cancelled = 2, Placed = 3
                // Assigned = 4, PickedUp = 5, Delivered = 6, Batched = 7, Closed = 8
                if (order.foodReady > 0) {
                    // how much time is left
                    // redo the pseudo button if there is time still assigned
                    // calc # minutes left, eg. 13

                    runPseudoButton(order, holder.rowView)

                    val btnAddTime = holder.rowView.btnAddTime
                    val btnRFP = holder.rowView.btnRFP

                    btnAddTime.setOnClickListener {
                        it.isEnabled = false // prevent mult clicks
                        AddTimeDialog().showDialog(activity, position, order)
                    }

                    btnRFP.setOnClickListener {
                        // update the order with timestamp = now(utc) + assigned time
                        val readyAt = DateTime(DateTimeZone.UTC)

                        if (!bRFP) {
                            Amplitude.getInstance().logEvent("Taps Ready for Pickup")
                            apiBridge.setReadyAt(order.id, readyAt, true)
                        }

                        apiBridge.onOrderUpdated = { orderInbound: Order ->
                            order.foodReady = orderInbound.foodReady
                            order.placed_on = orderInbound.placed_on
                            order.pickedup_on = orderInbound.pickedup_on
                            order.modified_on = orderInbound.modified_on

                            // this is for the trigger. It gets overwritten by the cloud.
                            order.status = "RFP"
                            dbBridge.updateOrderDB(order)

                            // toggle UI elements
                            imgPreparing.visibility = View.GONE
                            imgOMW.visibility = View.VISIBLE
                            btnRFP.visibility = View.GONE
                            addTimeUI(btnAddTime)
                            textViewOrderStatus.text = holder.rowView.context.resources.getString(R.string.ready_for_pickup)
                        }

                        apiBridge.onOrderUpdateFailed = { t: Throwable ->
                            AddTimeDialog().onOrderUpdateFailed(activity, t)
                        }
                    }
                } else {
                    textViewOrderStatus.text = holder.rowView.context.resources.getString(R.string.status_needs_estimate)
                    btnAssignTime.visibility = View.VISIBLE

                    // make button swell
                    val scaleUp = ObjectAnimator.ofPropertyValuesHolder(btnAssignTime,
                            PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                            PropertyValuesHolder.ofFloat("scaleY", 1.1f))
                    scaleUp.setDuration(1000)
                    scaleUp.repeatCount = 5 // ObjectAnimator.INFINITE
                    scaleUp.repeatMode = ObjectAnimator.REVERSE
                    scaleUp.start()
                }

                btnAssignTime.setOnClickListener {
                    mClickListener?.onPositionClicked(nAdapterPosition)
                }

                // 'button' may not be visible yet
                val pseudoButton = holder.rowView.findViewById<RelativeLayout>(R.id.pseudoButton)
                pseudoButton?.setOnClickListener {
                    mButtonListener?.onButtonClicked(nAdapterPosition)
                }

                holder.rowView.setOnClickListener {
                    mClickListener?.onPositionClicked(nAdapterPosition)
                }
            }
            else -> {
                // the 'no current orders' screen is one row,
                // so zero orders equates to one row. So the itemcount is padded by 1.
                // which is the 'spare' row, when there are orders.
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    // Plus one for the 'no orders' row.
    override fun getItemCount() = dbBridge.getOpenOrderCount() + 1

    override fun getItemViewType(position: Int): Int {
        return if (itemCount == 1) VIEW_TYPE.EMPTY.code else {
            // the last row is a spare, since we've padded by 1
            if (position == itemCount - 1) VIEW_TYPE.SPARE.code else VIEW_TYPE.NOT_EMPTY.code
        }
    }

    private fun runPseudoButton(order: Order, hr: RelativeLayout) {
        var updateButtonTask: Runnable? = null
        var pctRemaining = 1f
        val assignedTimeInMillis = order.foodReady * 60000

        if (assignedTimeInMillis == 0) return

        val btnPseudo = hr.pseudoButton
        val btnAssignTime = hr.btnAssignTime
        val btnRFP = hr.btnRFP
        val btnAddTime = hr.btnAddTime
        val imgPreparing = hr.imgPreparing
        val imgOMW = hr.imgOMW

        // hide previous 'assign time' button and show new pseudo 'button'
        btnAssignTime.visibility = View.GONE
        btnPseudo.visibility = View.VISIBLE

        val imgview = btnPseudo.findViewById<ImageView>(R.id.imgview)
        // because the element is nearly (or newly) visible, width may be reported as 0
        val maxWidth = btnPseudo.context.resources.getDimension(R.dimen.pseudo_button_width)
        val pbText = btnPseudo.findViewById<TextView>(R.id.progressText)

        // Log.i(javaClass.name, "runPseudoButton: order.placed_on:" + order.placed_on.toString())

        val handler = android.os.Handler()
        updateButtonTask = Runnable {
            val timeRemaining = Common.timeRemaining(order)
            val bRFP = dbBridge.isRFP(order)

            // Log.i(javaClass.name, "runPseudoButton: readyAt:" + readyAt.toString())
            // Log.i(javaClass.name, "runPseudoButton: timeRemaining:" + timeRemaining.toString())

            activity.runOnUiThread({
                val textViewOrderStatus = hr.textViewOrderStatus

                if (timeRemaining <= 0) {
                    // 00:00. pctRemaining = 1 = 100%
                    pctRemaining = 1f
                    pbText.text = Common.mmSS(0)

                    if (!bRFP) {
                        btnRFP.visibility = View.VISIBLE
                        btnAddTime.visibility = View.VISIBLE
                        btnPseudo.visibility = View.GONE

                        textViewOrderStatus.text = hr.context.resources.getString(R.string.ready_for_pickup_question)
                    }
                } else {
                    if (!bRFP) {
                        // show button width corresponding to original time set
                        pctRemaining = timeRemaining.toFloat() / assignedTimeInMillis.toFloat()
                        pbText.text = Common.mmSS(timeRemaining)
                        handler.postDelayed(updateButtonTask, 500)
                    }
                }

                if (bRFP) {
                    btnRFP.visibility = View.GONE
                    btnAddTime.visibility = View.VISIBLE
                    btnPseudo.visibility = View.GONE
                    imgPreparing.visibility = View.GONE
                    imgOMW.visibility = View.VISIBLE

                    addTimeUI(btnAddTime)

                    textViewOrderStatus.text = hr.context.resources.getString(R.string.ready_for_pickup)
                }

                val newWidth = (pctRemaining * maxWidth).roundToInt()

                /* Log.i(javaClass.name, "runPseudoButton: pctRemaining:" + pctRemaining.toString())
                Log.i(javaClass.name, "runPseudoButton: pbText:" + pbText.text.toString())
                Log.i(javaClass.name, "runPseudoButton: newWidth:" + newWidth.toString()) */

                // create new layoutparams with new width
                val p = RelativeLayout.LayoutParams(newWidth, RelativeLayout.LayoutParams.MATCH_PARENT)
                imgview.layoutParams = p
            })
        }

        handler.post(updateButtonTask)
    }

    private fun addTimeUI(btnAddTime: Button) {
        val btnLP = RelativeLayout.LayoutParams(btnAddTime.layoutParams)
        btnLP.addRule(RelativeLayout.ALIGN_PARENT_END)
        btnLP.addRule(RelativeLayout.CENTER_VERTICAL)
        btnLP.setMargins(0, 0, 5, 0)
        btnAddTime.layoutParams = btnLP
    }
}

