package biz.deliverydudes.vendorapp.recyclerViews

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.Order
import com.amplitude.api.Amplitude

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TabOrders.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TabOrders.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class TabHistory : Fragment() {
    lateinit var rv: RecyclerView
    lateinit var refreshHistory: SwipeRefreshLayout
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var ctx: Context
    private lateinit var apiBridge: APIBridge
    private lateinit var dbBridge: DBBridge
    private var orderBy = "DESC" // "ASC", or "DESC". Defaults to DESC

    companion object {
        var runIn5sHandler: android.os.Handler = Handler()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ctx = VendorContextWrapper(this.activity!!.applicationContext)
        apiBridge = APIBridge(ctx)
        dbBridge = DBBridge(ctx)
    }

    private fun onHistoricalOrdersRefreshed() {
        activity?.runOnUiThread {
            (rv.adapter as DataAdapterOrdersHistory).dataset = dbBridge.getHistoricalOrders(orderBy)
            rv.adapter.notifyDataSetChanged()
            refreshHistory.isRefreshing = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v: View = inflater.inflate(R.layout.fragment_orders_history, container, false)
        rv = v.findViewById<RecyclerView>(R.id.orders_recycler2)
        refreshHistory = v.findViewById<SwipeRefreshLayout>(R.id.refreshHistory)

        viewManager = LinearLayoutManager(this.context)

        viewAdapter = DataAdapterOrdersHistory(this.activity!!, object : OrdersRecyclerClickListener {
            override fun onPositionClicked(position: Int) {
                val indivOrder = Intent(context, OrderActivity::class.java)
                indivOrder.putExtra("mode", Order.Mode.History.code)
                indivOrder.putExtra("position", position)
                indivOrder.putExtra("orderBy", orderBy)
                startActivity(indivOrder)
            }

            override fun onLongClicked(position: Int) { }
        })

        rv.apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
            addItemDecoration(DividerItemDecoration(ctx, DividerItemDecoration.VERTICAL))
        }

        refreshHistory.setOnRefreshListener {
            Amplitude.getInstance().logEvent("User swipes to refresh")

            onHistoricalOrdersRefreshed()
        }

        val colReceivedAt = v.findViewById<TextView>(R.id.colReceivedAt)

        colReceivedAt?.setOnClickListener {
            // "DESC" --> "ASC" --> "DESC" ... etc.
            orderBy = if (orderBy == "DESC") "ASC" else "DESC"
            onHistoricalOrdersRefreshed()
        }

        // now that the rv has been late-inited.
        // and 5s is plenty of time to get the historical orders from the API.
        // after that, every 10 minutes is fine.
        var runIn5s: Runnable? = null
        runIn5s = Runnable {
            onHistoricalOrdersRefreshed()

            runIn5sHandler.postDelayed(runIn5s, 10 * 60 * 1000)
        }

        runIn5sHandler.postDelayed(runIn5s, 5 * 1000)

        return v
    }
}
