package biz.deliverydudes.vendorapp.recyclerViews

interface OrdersRecyclerClickListener {
    fun onPositionClicked(position: Int): Unit
    fun onLongClicked(position: Int): Unit
}

interface OrdersRecyclerButtonClickListener {
    fun onButtonClicked(position: Int): Unit
}
