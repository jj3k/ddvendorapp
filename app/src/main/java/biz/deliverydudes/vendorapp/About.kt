package biz.deliverydudes.vendorapp

import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Toast
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.DDAppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_about.*
import org.joda.time.DateTime
import java.io.File

class About : DDAppCompatActivity(DDAppCompatActivity.activityCode.About.code) {
    private val wrappedContext = VendorContextWrapper(this)
    private var ripCordClickCount = 0
    private var bSent = false
    lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        // set the title then make the toolbar the action bar
        val toolbar_title = findViewById(R.id.toolbar) as android.support.v7.widget.Toolbar
        val strAbout = resources.getString(R.string.about_the_vendor_app)
        toolbar_title.setTitle(strAbout)

        setSupportActionBar(toolbar_title)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        email = Common(wrappedContext).getFromSharedPrefs(Common.cache.Email.code)
        txtEmailAddress.text = email

        val pInfo = this.packageManager.getPackageInfo(packageName, 0)
        val version = "Version " + pInfo.versionName + (if (BuildConfig.DEBUG) "D" else "P")
        txtAppVersion.text = version
        txtReleaseNotes.text = Html.fromHtml(Common(wrappedContext).loadRawResource(R.raw.release_notes))
    }

    fun ripCord(v: View) {
        val nLeft = 6 - ripCordClickCount

        // emergency rip cord
        // pushes db and log files to API
        if (++ripCordClickCount > 6 && !bSent) {
            val fbStorage = FirebaseStorage.getInstance().reference
            val vendorID = Common(wrappedContext).getIntFromSharedPrefs(Common.cache.VendorID.code)

            // append database file to two 'daily' files (see ProdDBuggerInterceptor)
            val filenameYesterday = DateTime().plusDays(-1).toString("YYYY-MM-dd") + ".txt"
            val filenameToday = DateTime().toString("YYYY-MM-dd") + ".txt"
            val fileYesterday = File(wrappedContext.filesDir.absolutePath, filenameYesterday)
            val fileToday = File(wrappedContext.filesDir.absolutePath, filenameToday)

            val dbFile = wrappedContext.getDatabasePath("TrafficControl.db")
            val filesInFolder = arrayListOf<File>(dbFile)

            if (fileYesterday.exists()) filesInFolder.add(fileYesterday)
            if (fileToday.exists()) filesInFolder.add(fileToday)

            for (localFile: File in filesInFolder) {
                // / TODO remove email when vendor id is assuredly no longer zero.
                val path = if (vendorID == 0) email.split("@")[0] else vendorID.toString()
                val fileFB = fbStorage.child(path + "/" + localFile.name)

                fileFB.putFile(Uri.fromFile(localFile))
                    .addOnSuccessListener {
                        Log_LogEntry(LogEntry(context = wrappedContext,
                            message = "File ${fileFB.name} successfully sent to firebase storage.",
                            sourceClass = javaClass.name,
                            sourceMethod = "ripCord"))

                        bSent = true
                    }
                    .addOnFailureListener {
                        Log_LogEntry(LogEntry(context = wrappedContext,
                            message = "There was an issue sending file ${fileFB.name} to firebase storage (does it exist?)",
                            sourceClass = javaClass.name,
                            sourceMethod = "ripCord",
                            t = it))
                    }
            }

            Toast.makeText(wrappedContext, "Log information has been sent to Delivery Dudes! Thank you for helping!", Toast.LENGTH_LONG).show()
        } else if (bSent) {
            Toast.makeText(wrappedContext, "Log information has already been sent. Thanks again!", Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(wrappedContext, "Please tap $nLeft more times to send log information.", Toast.LENGTH_SHORT).show()
        }
    }
}
