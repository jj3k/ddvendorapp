package biz.deliverydudes.vendorapp

import android.content.Context
import android.widget.Toast
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.APIService
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.Network.NoConnectionException
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.ITB
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import biz.deliverydudes.vendorapp.models.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.ISODateTimeFormat
import retrofit2.Call
import retrofit2.Response
import java.net.SocketTimeoutException

/*
    This layer is above above the API layer.
    All application calls to the API come here.
 */

class APIBridge(private val ctx: Context) {
    private val context = VendorContextWrapper(ctx)
    private var ddAPIService: APIService = APIService.createAPIService(context)
    private val dbBridge = DBBridge(ctx)

    // These will NOT be invoked on the UI/Main thread. It is up to the
    // implementation to execute on the UI/Main thread if needed.
    // var onOrdersRefreshed: (() -> Unit)? = null
    var onLoginSucceeded: (() -> Unit)? = null
    var onLoginFailed: ((t: Throwable) -> Unit)? = null
    var onOrderUpdated: ((o: Order) -> Unit)? = null
    var onOrderUpdateFailed: ((t: Throwable) -> Unit)? = null
    var onPingSucceeded: (() -> Unit)? = null
    var onPingFailed: ((t: Throwable) -> Unit)? = null
    var onGetVendorInfoSucceeded: ((User) -> Unit)? = null
    var onForgotPasswordSucceeded: ((Response<ResponseBody>) -> Unit)? = null
    var onAddOrDeleteITBUser: ((Boolean, String) -> Unit)? = null

    fun ping() {
        // ping first
        val savedCookie = Common(context).getFromSharedPrefs(Common.cache.Cookie.code)
        if (savedCookie.isNotEmpty()) {
            ddAPIService.ping()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        onPingSucceeded?.invoke()
                    }, {
                        onPingFailed?.invoke(it)
                    })
        }
    }

    fun getVendorInfo() {
        ddAPIService.getVendorAccount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onGetVendorInfoSucceeded?.invoke(it)
                }, {
                    onAPIFailure(it)
                })
    }

    // failure for forgot password, ping and for setVendorInfo (GET /vendor_app/account)
    fun onAPIFailure(thr: Throwable) {
        if (thr is NoConnectionException)
            Toast.makeText(ctx, ctx.resources.getString(R.string.no_network_or_api_down), Toast.LENGTH_LONG).show()

        Log_LogEntry(LogEntry(context = context,
                http_request_base_url = APIService.API_URL,
                error_module = javaClass.name,
                error_submodule = "onAPIFailure",
                message = "Perhaps login is no good, or network is inaccessible.",
                t = thr))
    }

    fun login(email: String, password: String, android_id: String, os_version: String, app_version: String) {
        ddAPIService.login(email, password, android_id, app_version, os_version)
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    onLoginSucceeded?.invoke()
                }, {
                    onLoginFailed?.invoke(it)
                })
    }

    fun logout() {
        dbBridge.database.logout()

        ddAPIService.logout()
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    Log_LogEntry(LogEntry(context = context,
                            http_response_body = it.string(),
                            message = "Logging out now ... BYE!"))
                }, {
                    Log_LogEntry(LogEntry(context = context,
                            http_request_base_url = APIService.API_URL + APIService.url_base_logout,
                            sourceClass = javaClass.name,
                            sourceMethod = "logout",
                            message = "",
                            t = it))
                })
    }

    fun forgotPassword(email: String) {
        ddAPIService.forgotPassword(email)
                .enqueue(object : retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onAPIFailure(t
                                ?: Throwable("An unknown error was encountered in forgotPassword."))
                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response != null)
                            onForgotPasswordSucceeded?.invoke(response)
                    }
                })
    }

    fun setReadyAt(nOrderID: Int, readyAt: DateTime, duration_update: Boolean) {
        // duration_update is true only when food is "READY FOR PICKUP"
        val formatter = ISODateTimeFormat.dateTime()
        val readyAtURL = formatter.print(readyAt)

        ddAPIService.setReadyAt(nOrderID, readyAtURL, duration_update = if (duration_update) "true" else "false")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    onOrderUpdated?.invoke(it)
                }, {
                    onOrderUpdateFailed?.invoke(it)
                })
    }

    fun getAPIOrder(nOrderID: Int, cb: (o: Order) -> Unit) {
        ddAPIService.getOrder(nOrderID)
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    dbBridge.saveOrderToDB(it)
                    cb(it)
                }, {
                    getOrderFailure(nOrderID, it)
                })
    }

    fun sendFeedback(
        type: String,
        message: String,
        nOrderID: Int,
        app_version: String,
        os_version: String,
        successCB: ((r: ResponseBody) -> Unit),
        failureCB: ((t: Throwable) -> Unit)
    ) {
        val fbRequest = ddAPIService.sendFeedback(
                feedback_type = type,
                message = message,
                app_version = BuildConfig.VERSION_NAME,
                os_version = os_version,
                order_id = if (nOrderID == 0) dbBridge.assumeLastOrderAsReference() else nOrderID)

        fbRequest.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    successCB(it)
                }, {
                    failureCB(it)
                })
    }

    // refresh orders reloads all orders for the past 2 weeks
    // this is a direct translation of the Qt creator queuecontroller::getOrders
    fun getAPIOrders(nOffset: Int, refreshUI: (() -> Unit)) {
        val limit = ctx.resources.getInteger(R.integer.max_orders_from_cloud)
        val lastOrdersRetrievedAt = dbBridge.ordersTable.getLastOrdersRetrievedAt()
        val nowMinusOneMonth = DateTime(DateTimeZone.UTC).plusMonths(-1)
        val formatter = ISODateTimeFormat.dateTime()
        val since: String = formatter.print(nowMinusOneMonth)

        if (nOffset == 0 && lastOrdersRetrievedAt != "") {
            val updated_after = formatter.print(DateTime(lastOrdersRetrievedAt, DateTimeZone.UTC))

            ddAPIService.getOrders(updated_after)
                    .subscribeOn(Schedulers.io())
                    ?.observeOn(AndroidSchedulers.mainThread())
                    ?.subscribe({
                        dbBridge.saveOrdersToDB(it, refreshUI)
                    }, {
                        getOrdersFailure(it, refreshUI)
                    })
        } else {
            if (nOffset > 0) {
                ddAPIService.getOrders(nOffset, since, limit)
                        .subscribeOn(Schedulers.io())
                        ?.observeOn(AndroidSchedulers.mainThread())
                        ?.subscribe({
                            dbBridge.saveOrdersToDB(it, refreshUI)
                        }, {
                            getOrdersFailure(it)
                        })
            } else {
                // lastOrdersRetrievedAt was never set, it's their first run ever
                ddAPIService.getOrders(since, limit, "desc")
                        .subscribeOn(Schedulers.io())
                        ?.observeOn(AndroidSchedulers.mainThread())
                        ?.subscribe({
                            dbBridge.saveOrdersToDB(it, refreshUI)
                        }, {
                            getOrdersFailure(it)
                        })
            }
        }
    }

    private fun getOrderFailure(nOrderID: Int, inboundError: Throwable) {
        // chances are good we got a 404
        // chances are GREAT the order was picked (or possibly cancelled)
        dbBridge.updateOrderDBAsPicked(nOrderID, {})

        Log_LogEntry(LogEntry(context = context,
                http_request_base_url = APIService.API_URL + APIService.url_base_order,
                error_module = javaClass.name,
                error_submodule = "getOrderFailure",
                message = "$nOrderID entered a fugue state (is a zombie order), and was marked as picked.",
                t = inboundError))
    }

    private fun getOrdersFailure(inboundError: Throwable) {
        getOrdersFailure(inboundError, {})
    }

    private fun getOrdersFailure(inboundError: Throwable, callback: (() -> Unit)) {
        try {
            if (inboundError is retrofit2.HttpException) {
                val code = inboundError.code()
                val body = inboundError.response().body() ?: ""

                // 304 not modified is ok
                if (code == 404 && body == "") {
                    // this is still ok, so long as the response body is empty
                    callback()
                } else if (code != 304) {
                    onAPIFailure(inboundError)
                } else if (code == 304) {
                    callback()
                }
            } else {
                var message = "A non-HttpException was received."

                try {
                    val inboundErrorAsSocketTimeout = (inboundError as SocketTimeoutException)
                    message = "Network timed out."
                } catch (ex: Exception) {
                }

                Log_LogEntry(LogEntry(context = context,
                        http_request_base_url = APIService.API_URL + APIService.url_base_order,
                        error_module = javaClass.name,
                        error_submodule = "getOrdersFailure",
                        message = message,
                        t = inboundError))
            }
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = context,
                    http_request_base_url = APIService.API_URL + APIService.url_base_order,
                    error_module = javaClass.name,
                    error_submodule = "getOrdersFailure",
                    message = "An unknown error occurred.",
                    t = ex))
        }
    }

    // region ITB
    fun addITBInAPI(email: String): ITB {
        val addITB = ITB()
        addITB.email_address = email
        val itbsCreateRequest = ddAPIService.addITBinAPI(email)

        itbsCreateRequest.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    addITB.id = it.id
                    addITB.vendor_id = it.vendor_id
                    addITB.first_name = it.first_name
                    addITB.last_name = it.last_name

                    dbBridge.itbTable.insertITB(addITB)
                    onAddOrDeleteITBUser?.invoke(true, "${it.first_name} ${it.last_name}")
                }, { t ->
                    onAddOrDeleteITBUser?.invoke(false, "")

                    Log_LogEntry(LogEntry(context = context,
                            message = "An error was encountered adding an in-the-business account in the API. ${t.message}"))
                })

        return addITB
    }

    fun deleteITBInAPI(id: Int, email: String) {
        val itbsDeleteRequest = ddAPIService.deleteITBinAPI(id, email)

        itbsDeleteRequest.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    dbBridge.itbTable.deleteITBinDB(email)
                    onAddOrDeleteITBUser?.invoke(true, email)
                }, { t ->
                    onAddOrDeleteITBUser?.invoke(false, email)
                    Log_LogEntry(LogEntry(context = context,
                            message = "An error was encountered deleting an in-the-business account in the API. ${t.message}"))
                })
    }

    fun getAllITBEmailsAPI() {
        val itbsRequest = ddAPIService.getAllITBEmails()

        itbsRequest.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    dbBridge.itbTable.insertITBs(it)
                }, { t ->
                    Log_LogEntry(LogEntry(context = context,
                            message = "An error was encountered getting all in-the-business accounts from the API. ${t.message}"))
                })
    }

    // endregion
}
