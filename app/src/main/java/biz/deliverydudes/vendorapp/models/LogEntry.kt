package biz.deliverydudes.vendorapp.models

import android.content.Context
import android.util.Log
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.helpers.Common

class LogEntry constructor(
    val context: Context,
    var api_level: String = android.os.Build.VERSION.SDK_INT.toString(),
    var app_version: String = BuildConfig.VERSION_NAME,
    var http_request_base_url: String = "",
    var http_request_type: String = "",
    var http_response_body: String = "",
    var http_response_code: String = "",
    var last_orders_retrieved_at: String = "",
    var error_message: String = "",
    var error_module: String = "",
    var error_submodule: String = "",
    var error_type: String = "",
    var message: String = "",
    var os: String = "Android",
    var os_version: String = android.os.Build.VERSION.RELEASE,
    var params: String = "",
    var sourceClass: String = "",
    var sourceMethod: String = "",
    var t: Throwable? = null
) {
    val vendor_id: Int = Common(context = context).getIntFromSharedPrefs(Common.cache.VendorID.code)
    val email: String = Common(context = context).getFromSharedPrefs(Common.cache.Email.code)
    val android_id: String = Common(context = context).getFromSharedPrefs(Common.cache.AndroidID.code)

    init {
        var bError: Boolean = false
        try {
            checkNotNull(t)

            bError = true
            this.error_message = this.t?.message.toString()
        } catch (ex: IllegalStateException) {
            this.error_message = ""
        }

        if (BuildConfig.DEBUG) if (this.message.isNotBlank()) Log.i(this.sourceClass + "." + this.sourceMethod, this.message)
        if (BuildConfig.DEBUG) if (bError) Log.e(this.error_module, this.error_submodule, this.t)
    }

    companion object {
        // static log -- where there is no context available.
        fun log(sourceClass: String, sourceMethod: String, errMessage: String) {
            val bError = errMessage.isNotBlank()

            if (BuildConfig.DEBUG)
                if (errMessage.isNotBlank())
                    Log.i("$sourceClass.$sourceMethod", errMessage)
            if (BuildConfig.DEBUG && bError)
                Log.e(sourceClass, "Error message: $sourceMethod $errMessage")
        }
    }

    override fun toString(): String {
        return "api_level=$api_level " +
            "android_id=$android_id " +
            "app_version=$app_version " +
            "email=$email " +
            "http_request_base_url=$http_request_base_url " +
            "http_request_type=$http_request_type " +
            "http_response_body=$http_response_body " +
            "http_response_code=$http_response_code " +
            "last_orders_retrieved_at=$last_orders_retrieved_at " +
            (if (error_message == "") "" else "error_message=$error_message ") +
            (if (error_module == "") "" else "error_module=$error_module ") +
            (if (error_submodule == "") "" else "error_submodule=$error_submodule ") +
            (if (error_type == "") "" else "error_type=$error_type ") +
            "message=$message " +
            "os=$os " +
            "os_version=$os_version " +
            "params=$params " +
            "vendor_id=$vendor_id"
    }
}