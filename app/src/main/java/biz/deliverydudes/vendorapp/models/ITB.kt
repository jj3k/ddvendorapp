package biz.deliverydudes.vendorapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ITB : Serializable {
    @SerializedName("id")
    var id = 0

    @SerializedName("vendor_id")
    var vendor_id = 0

    @SerializedName("email")
    var email_address = ""

    @SerializedName("first_name")
    var first_name = ""

    @SerializedName("last_name")
    var last_name = ""
}