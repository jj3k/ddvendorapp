package biz.deliverydudes.vendorapp.models

import com.amplitude.api.Revenue
import com.google.gson.annotations.SerializedName
import org.json.JSONArray
import org.json.JSONObject

// Submitted = 0
// Confirmed = 1
// Cancelled = 2
// Placed = 3
// Assigned = 4
// PickedUp = 5
// Delivered = 6
// Batched = 7
// Closed = 8

class Order(
    @SerializedName("id") var id: Int,
    @SerializedName("food_ready") var foodReady: Int,
    @SerializedName("status") var status: String,
    @SerializedName("cart_items") var items: ArrayList<CartItem>,
    @SerializedName("pickedup_on") var pickedup_on: String,
    var placed_on: String,
    var total: String = "0.0",
    @SerializedName("tax") var tax: Double,
    @SerializedName("modified_on") var modified_on: String
) {

    enum class Status(val code: String) {
        Submitted("submitted"),
        Confirmed("confirmed"),
        Cancelled("cancelled"),
        Placed("placed"),
        Assigned("assigned"),
        PickedUp("pickedup"),
        Delivered("delivered"),
        Batched("batched"),
        Closed("closed");

        companion object {
            fun from(getCode: String): Status {
                var statusReturn = Status.Placed

                try {
                    statusReturn = Status.values().first { it.code.toUpperCase() == getCode.toUpperCase() }
                } catch (ex: NoSuchElementException) {
                    // one of the kluge statuses: RFP, UNRFP, they're just for the triggers
                }

                return statusReturn
            }
        }
    }

    enum class Mode(val code: String) {
        Current("current"),
        History("history");

        companion object {
            fun from(getCode: String): Mode = Mode.values().first { it.code.toUpperCase() == getCode.toUpperCase() }
        }
    }

    override fun toString(): String {
        return this.toJSONObject().toString()
    }

    private fun toJSONObject(): JSONObject {
        val json: JSONObject = JSONObject()
        json.put("id", this.id)
        json.put("food_ready", this.foodReady)
        json.put("status", this.status)

        var i = 0
        val cartItems = JSONArray()
        this.items.forEach {
            cartItems.put(i++, it.toJSONObject())
        }

        json.put("cart_items", cartItems)
        json.put("pick_time", this.pickedup_on)
        json.put("placed_on", this.placed_on)
        json.put("total", this.total)
        json.put("tax", this.tax.toString())
        json.put("modified_on", this.modified_on.toString())

        return json
    }

    fun isEmpty(): Boolean {
        // no order is allowed to have an empty cart
        val bReturn = this.id == 0 &&
                this.foodReady == 0 &&
                this.status == "" &&
                this.placed_on == ""

        return bReturn
    }

    fun getRevenueObject(): Revenue {
        return Revenue()
                .setProductId(this.id.toString())
                .setPrice(this.total.toDouble() / 100.0)
                .setQuantity(this.items.count())
    }

    fun count(): Int {
        return this.items.sumBy { it.quantity ?: 0 }
    }
}
