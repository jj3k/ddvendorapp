package biz.deliverydudes.vendorapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class User : Serializable {
    @SerializedName("id")
    var id = 0

    @SerializedName("billing_address_id")
    var billing_address_id = 0

    @SerializedName("billing_email")
    var billing_email = ""

    @SerializedName("email")
    var email = ""

    @SerializedName("first_name")
    var first_name = ""

    @SerializedName("last_name")
    var last_name = ""

    @SerializedName("phone")
    var phone = ""

    @SerializedName("role")
    var user_role = 0

    @SerializedName("territory_id")
    var territory_id = 0

    @SerializedName("vendor_id")
    var vendor_id = 0

    @SerializedName("in_the_biz_cap")
    var in_the_biz_cap = 0
}