package biz.deliverydudes.vendorapp.models

import org.json.JSONObject

class CartItem() {
    var cart_item_id: Int? = null
    var categoryName: String? = null
    var productId: Int? = null
    var productName: String? = null
    var additionalInstructions: String? = null
    var productPrice: String? = null
    var quantity: Int? = null

    constructor(
        cart_item_id: Int,
        productId: Int,
        categoryName: String,
        productName: String,
        additionalInstructions: String,
        productPrice: String,
        quantity: Int
    ): this() {

        this.cart_item_id = cart_item_id
        this.categoryName = categoryName
        this.productId = productId
        this.productName = productName
        this.additionalInstructions = additionalInstructions
        this.productPrice = productPrice
        this.quantity = quantity
    }

    fun toJSONObject(): JSONObject {
        val json = JSONObject()
        json.put("cart_item_id", this.cart_item_id)
        json.put("category_name", this.categoryName)
        json.put("product_id", this.productId)
        json.put("product_name", this.productName)
        json.put("additional_instructions", this.additionalInstructions)
        json.put("product_price", this.productPrice)
        json.put("quantity", this.quantity)

        return json
    }
}
