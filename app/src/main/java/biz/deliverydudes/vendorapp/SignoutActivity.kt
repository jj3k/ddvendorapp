package biz.deliverydudes.vendorapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.Network.PusherClient
import biz.deliverydudes.vendorapp.helpers.Network.PusherWebSocket
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.recyclerViews.TabOrders
import com.amplitude.api.Amplitude

class SignoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Amplitude.getInstance().logEvent("Selects Logout")

        setContentView(R.layout.activity_signout)

        val wrappedContext = VendorContextWrapper(this)

        Log_LogEntry(LogEntry(context = wrappedContext,
                message = "Signing out of pusher channels (native and websocket): ${PusherClient.vendorChannel}"))
        PusherClient.signout()
        PusherWebSocket.signout()

        // 3-minute API poller
        TabOrders.pollOrdersHandler.removeCallbacksAndMessages(null)

        Common(wrappedContext).clearSharedPrefs()
        Common(wrappedContext).stfu()

        finishAffinity()

        val restart = Intent(this, SigninActivity::class.java)
        startActivity(restart)
    }
}
