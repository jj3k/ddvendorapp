package biz.deliverydudes.vendorapp.helpers.Database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import android.util.Log
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.CartItem
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

enum class CountMode {
    Orders,
    History
}

class OrdersTable(private val context: Context) {
    companion object {
        const val TABLE_NAME = "orders"
        const val COLUMN_ORDER_ID = "id"
        const val COLUMN_FOOD_READY = "food_ready"
        const val COLUMN_STATUS = "status"
        const val COLUMN_PICK_TIME = "pick_time"
        const val COLUMN_PLACED_ON = "placed_on"
        const val COLUMN_TOTAL = "total"
        const val COLUMN_MODIFIED_ON = "modified_on"

        // Orders
        const val SQL_CREATE_ORDERS_TABLE = """CREATE TABLE IF NOT EXISTS orders
            (
                id BIG INT PRIMARY KEY NOT NULL,
                food_ready INTEGER,
                status TEXT,
                pick_time DATE,
                placed_on DATE,
                total TEXT,
                modified_on TEXT
            )"""

        // Current Orders
        val SQL_SELECT_CURRENT_ORDERS = "SELECT " +
            "id, food_ready, placed_on, status, pick_time, total, modified_on " +
            "FROM orders " +
            "WHERE status = '${Order.Status.Placed.code}' " +
            "OR status = '${Order.Status.Assigned.code}' " +
            "OR status = 'RFP' OR status = 'UNRFP' "

        // Count current orders
        val SQL_SELECT_COUNT_ORDERS = "SELECT COUNT(id) " +
            "FROM orders " +
            "WHERE status = '${Order.Status.Placed.code}' " +
            "OR status = '${Order.Status.Assigned.code}' " +
            "OR status = 'RFP' OR status = 'UNRFP' "

        // Historical orders
        val SQL_SELECT_ORDERS_HISTORY = "SELECT " +
            "id, food_ready, status, pick_time, placed_on, total, modified_on " +
            "FROM orders " +
            "WHERE status != '${Order.Status.Placed.code}' " +
            "AND status != '${Order.Status.Assigned.code}' " +
            "AND status != '${Order.Status.Cancelled.code}' " // trailing spaces

        // Count historical orders
        val SQL_SELECT_COUNT_ORDERS_HISTORY = "SELECT COUNT(id) " +
            "FROM orders " +
            "WHERE status != '${Order.Status.Placed.code}'" +
            "AND status != '${Order.Status.Assigned.code}' " +
            "AND status != '${Order.Status.Cancelled.code}' "

        val SQL_SELECT_ORDERS_HISTORY_PLACED_ON_ASC = SQL_SELECT_ORDERS_HISTORY + "ORDER BY placed_on ASC"

        val SQL_SELECT_ORDERS_HISTORY_PLACED_ON_DESC = SQL_SELECT_ORDERS_HISTORY + "ORDER BY placed_on DESC"

        const val SQL_DELETE_ORDERS = "DELETE FROM orders"

        const val SQL_DROP_ORDERS = "DROP TABLE IF EXISTS orders"

        const val SQL_GET_LAST_ORDERS_RETRIEVED_AT = "SELECT MAX(modified_on) FROM orders"

        const val SQL_GET_MAX_ORDER_NUMBER = "SELECT MAX(id) FROM orders"

        // orders that are not picked but are older than 12 hours -- set = picked
        val SQL_AUTO_PICK_ORDERS_OVER_12HRS_OLD = "UPDATE orders SET status = '${Order.Status.PickedUp.code}' " +
            "WHERE (status = '${Order.Status.Placed.code}' " +
            "OR status = '${Order.Status.Assigned.code}' " +
            "OR status = 'RFP' OR status = 'UNRFP') " +
            "AND strftime('%Y-%m-%d %H:%M:%S', placed_on) <= datetime('now', '-12 hours')"

        const val SQL_CHECK_EXISTS_ORDERS = "SELECT 1 FROM sqlite_master WHERE type = 'table' AND tbl_name = 'orders' limit 1"
    }

    private val db = LocalDB.instance(context).writableDatabase

// region GET ORDER(S) OR CART ITEM(S)
    fun getOrder(orderID: Int): Order {
        val SQL_GET_ORDER = "SELECT " +
                "id, food_ready, status, pick_time, placed_on, modified_on, total " +
                "FROM orders " +
                "WHERE id = $orderID"

        return getOrderFromRawSQL(SQL_GET_ORDER)
    }

    fun getNthOrder(nth: Int, mode: Order.Mode, orderBy: String): Order {
        val SQL_GET_NTH_ORDER =
            (if (mode == Order.Mode.History)
                     if (orderBy == "DESC") SQL_SELECT_ORDERS_HISTORY_PLACED_ON_DESC
                     else SQL_SELECT_ORDERS_HISTORY_PLACED_ON_ASC
             else
                 SQL_SELECT_CURRENT_ORDERS) + " LIMIT 1 OFFSET $nth"

        return getOrderFromRawSQL(SQL_GET_NTH_ORDER)
    }

    private fun getOrderFromRawSQL(sql: String): Order {
        val orderReturn = Order(0, 0, "", arrayListOf(CartItem()), "", "", "", 0.0, "")

        try {
            db.beginTransaction()
            val cursor = db.rawQuery(sql, null)

            cursor?.use {
                if (it.moveToFirst()) {
                    orderReturn.apply {
                        id = it.getInt(it.getColumnIndex(COLUMN_ORDER_ID))
                        foodReady = it.getInt(it.getColumnIndex(COLUMN_FOOD_READY))
                        status = it.getString(it.getColumnIndex(COLUMN_STATUS))
                        items = getCartItemsForOrder(id)
                        placed_on = it.getString(it.getColumnIndex(COLUMN_PLACED_ON))
                        pickedup_on = it.getString(it.getColumnIndex(COLUMN_PICK_TIME)) ?: ""
                        total = it.getString(it.getColumnIndex(COLUMN_TOTAL))
                        modified_on = it.getString(it.getColumnIndex(COLUMN_MODIFIED_ON))
                    }
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getOrderFromRawSQL",
                t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }

        return orderReturn
    }

    private fun getCartItemsForOrder(nOrderID: Int): ArrayList<CartItem> {
        val cartItemsReturn = ArrayList<CartItem>()

        val SQL_GET_CART_ITEMS = "SELECT " +
                "id, additional_instructions, category_name, product_description, product_name, product_price, quantity " +
                "FROM cart_items " +
                "WHERE order_id = $nOrderID"
        val cursor: Cursor = db.rawQuery(SQL_GET_CART_ITEMS, null)

        try {
            db.beginTransaction()
            cursor.use {
                while (it.moveToNext()) {
                    val newCartItem = CartItem()

                    newCartItem.apply {
                        CartItemsTable.apply {
                            cart_item_id = it.getInt(it.getColumnIndex(ID))
                            categoryName = it.getString(it.getColumnIndex(CATEGORY_NAME))
                            productName = it.getString(it.getColumnIndex(PRODUCT_NAME))
                            additionalInstructions = it.getString(it.getColumnIndex(ADDITIONAL_INSTRUCTIONS))
                            productPrice = it.getString(it.getColumnIndex(PRODUCT_PRICE))
                            quantity = it.getInt(it.getColumnIndex(QUANTITY))
                        }
                    }

                    cartItemsReturn.add(newCartItem)
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getCartItemsForOrder",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return cartItemsReturn
    }

// endregion

// region HISTORY
    fun getHistoricalOrders(orderBy: String): ArrayList<Order> {
        val history = ArrayList<Order>()
        val cursor: Cursor = db.rawQuery(
                                if (orderBy == "DESC")
                                    SQL_SELECT_ORDERS_HISTORY_PLACED_ON_DESC
                                else
                                    SQL_SELECT_ORDERS_HISTORY_PLACED_ON_ASC, null)

        try {
            db.beginTransaction()

            cursor.use {
                if (it.moveToFirst()) {
                    while (!it.isAfterLast) {
                        // val newCartItem: CartItem = CartItem(null, null, null, null, null)
                        val newCartItem = arrayListOf(CartItem())
                        val newHistoryOrder = Order(0, 0, "", newCartItem, "", "", "", 0.0, "")

                        newHistoryOrder.apply {
                            id = it.getInt(it.getColumnIndex(COLUMN_ORDER_ID))
                            foodReady = it.getInt(it.getColumnIndex(COLUMN_FOOD_READY))
                            items = getCartItemsForOrder(id)
                            status = it.getString(it.getColumnIndex(COLUMN_STATUS))
                            placed_on = it.getString(it.getColumnIndex(COLUMN_PLACED_ON))
                            total = it.getString(it.getColumnIndex(COLUMN_TOTAL))
                        }

                        history.add(newHistoryOrder)
                        it.moveToNext()
                    }
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getHistoricalOrders",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return history
    }

    fun getOrderHistoryCount(): Int {
        return getCount(CountMode.History)
    }
// endregion

// region UPDATE ORDERS
    fun updateOrder(order: Order) {
        var rawSQL = ""
        var bSuccess = false

        try {
            // to update cart items, we'll empty the cart and
            // add the new items. Self-contained, transactionally.
            bSuccess = emptyCart(order)
            bSuccess = bSuccess && insertCartItems(order)

            if (!bSuccess) {
                Log_LogEntry(LogEntry(context = context,
                    message = "An unknown error occurred when refilling cart items in order #${order.id}.",
                    error_module = javaClass.name,
                    error_submodule = "updateOrder"))
            }

            // now for the order-level changes
            db.beginTransaction()

            order.apply {
                rawSQL = "UPDATE orders SET \n" +
                    " food_ready = $foodReady, \n" +
                    " status = '$status', \n" +
                    " pick_time = '$pickedup_on', \n" +
                    " placed_on = '$placed_on', \n " +
                    " modified_on = '$modified_on' " +
                    " WHERE id = $id"

                if (BuildConfig.DEBUG) Log.i(javaClass.name, rawSQL)
                val cursorOrder = db.rawQuery(rawSQL, null)

                cursorOrder.use {
                    it.moveToFirst()
                    bSuccess = bSuccess && it.count >= 0
                }
            }

            if (bSuccess)
                db.setTransactionSuccessful()
            else
                Log_LogEntry(LogEntry(context = context,
                    message = "An unknown error occurred when trying to run $rawSQL.",
                    error_module = javaClass.name,
                    error_submodule = "updateOrder"))
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "updateOrder",
                t = ex))
        } finally {
            db.endTransaction()
        }
    }

    // Update an order as picked in the db (called from Pusher)
    fun updateOrderAsPicked(nOrderID: Int) {
        val order = getOrder(nOrderID)

        updateOrder(order.apply {
            status = Order.Status.PickedUp.code
            pickedup_on = DateTime(DateTimeZone.UTC).toString()
            modified_on = DateTime(DateTimeZone.UTC).toString()
        })
    }

    // Automatically cancel orders older than 12 hours.
    fun autoCancel12HrOldOrders(): Boolean {
        var bSuccess = false

        try {
            db.beginTransaction()

            val cursor: Cursor = db.rawQuery(SQL_AUTO_PICK_ORDERS_OVER_12HRS_OLD, null)

            cursor.use {
                it.moveToFirst()
                bSuccess = it.count >= 0
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            bSuccess = false

            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "autoCancel12HrOldOrders",
                t = ex))
        } finally {
            // "please stop crashing"
            if (db.isOpen)
                db.endTransaction()
        }

        return bSuccess
    }

    // Cancel an order in the db (called from Pusher)
    fun cancelOrderById(nOrderID: Int): Boolean {
        val cursor: Cursor?
        var bReturnValue = false
        val SQL_CANCEL_ORDER = "UPDATE orders SET status = '${Order.Status.Cancelled.code}' WHERE id = $nOrderID"

        try {
            db.beginTransaction()

            cursor = db.rawQuery(SQL_CANCEL_ORDER, null)

            cursor?.use {
                bReturnValue = it.moveToFirst()
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "cancelOrderById",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return bReturnValue
    }

// endregion

    fun saveOrders(orders: ArrayList<Order>) {
        db.beginTransaction()

        try {
            for (order: Order in orders) {
                insertOrder(order)
            }

            db.setTransactionSuccessful()
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "saveOrders",
                t = ex))
        } finally {
            db.endTransaction()
        }
    }

    private fun emptyCart(o: Order): Boolean {
        var bSuccess = false

        try {
            db.beginTransaction()

            // update cart items: delete old, add new.
            val rawDeleteCartItems = "DELETE FROM cart_items WHERE order_id = ${o.id}"

            if (BuildConfig.DEBUG) Log.i(javaClass.name, rawDeleteCartItems)
            val cursor = db.rawQuery(rawDeleteCartItems, null)

            cursor.use {
                it.moveToFirst()
                bSuccess = it.count >= 0
            }

            db.setTransactionSuccessful()
        } catch (ex: Exception) {
            bSuccess = false

            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "emptyCart",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return bSuccess
    }

    private fun insertCartItems(o: Order): Boolean {
        var bSuccess = true

        try {
            db.beginTransaction()

            val cartItems = o.items

            for (cartItem in cartItems) {
                val cart_values = ContentValues()

                cart_values.apply {
                    put(CartItemsTable.ID, cartItem.cart_item_id)
                    put(CartItemsTable.ADDITIONAL_INSTRUCTIONS, cartItem.additionalInstructions)
                    put(CartItemsTable.CATEGORY_NAME, cartItem.categoryName)
                    put(CartItemsTable.PRODUCT_ID, cartItem.productId)
                    put(CartItemsTable.PRODUCT_NAME, cartItem.productName)
                    put(CartItemsTable.PRODUCT_PRICE, cartItem.productPrice)
                    put(CartItemsTable.QUANTITY, cartItem.quantity)
                    put(CartItemsTable.ORDER_ID, o.id)
                }

                db.insert(CartItemsTable.TABLE_NAME, null, cart_values)
            }

            db.setTransactionSuccessful()
        } catch (ex: Exception) {
            bSuccess = false

            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "insertCartItems",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return bSuccess
    }

    private fun insertOrder(order: Order): Boolean {
        val bIsOrderInDB = !getOrder(order.id).isEmpty()

        if (bIsOrderInDB) {
            updateOrder(order)
        } else {
            // cart items first
            insertCartItems(order)

            // insert
            // Create a new map of values, where column names are the keys
            val order_values = ContentValues()

            order_values.apply {
                put(COLUMN_ORDER_ID, order.id)
                put(COLUMN_FOOD_READY, order.foodReady)
                put(COLUMN_STATUS, order.status)
                put(COLUMN_PICK_TIME, order.pickedup_on)
                put(COLUMN_PLACED_ON, order.placed_on)
                put(COLUMN_TOTAL, order.total)
                put(COLUMN_MODIFIED_ON, order.modified_on)
            }

            try {
                db.beginTransaction()
                val newRowId = db.insert(TABLE_NAME, null, order_values)
                db.setTransactionSuccessful()
            } catch (ex: Exception) {
                Log_LogEntry(LogEntry(context = context,
                    error_module = javaClass.name,
                    error_submodule = "insertOrder",
                    t = ex))
            } finally {
                db.endTransaction()
            }
        }

        return true
    }

    fun getOrderCount(): Int {
        return getCount(CountMode.Orders)
    }

    private fun getCount(mode: CountMode): Int {
        var count = 0

        // straggler recyclerview event on signout
        if (!db.isOpen) {
            return count
        }

        try {
            db.beginTransaction()
            val cursor = db.rawQuery(if (mode == CountMode.Orders) SQL_SELECT_COUNT_ORDERS else SQL_SELECT_COUNT_ORDERS_HISTORY, null)

            cursor?.use {
                if (it.moveToFirst()) {
                    count = it.getInt(0)
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = if (mode == CountMode.Orders) "getCount Orders" else "getCount History",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return count
    }

    fun getOpenOrders(): ArrayList<Order> {
        val openOrders = ArrayList<Order>()
        val cursor: Cursor?

        try {
            db.beginTransaction()
            cursor = db.rawQuery(SQL_SELECT_CURRENT_ORDERS, null)

            cursor?.use {
                while (it.moveToNext()) {
                    val newCartItem = arrayListOf(CartItem())
                    val newOrder = Order(0, 0, "", newCartItem, "", "", "", 0.0, "")

                    newOrder.apply {
                        id = it.getInt(it.getColumnIndex(COLUMN_ORDER_ID))
                        foodReady = it.getInt(it.getColumnIndex(COLUMN_FOOD_READY))
                        status = it.getString(it.getColumnIndex(COLUMN_STATUS))
                        items = getCartItemsForOrder(id)
                        pickedup_on = it.getString(it.getColumnIndex(COLUMN_PICK_TIME)) ?: ""
                        placed_on = it.getString(it.getColumnIndex(COLUMN_PLACED_ON))
                        modified_on = it.getString(it.getColumnIndex(COLUMN_MODIFIED_ON))
                        total = it.getString(it.getColumnIndex(COLUMN_TOTAL))
                    }

                    openOrders.add(newOrder)
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getOpenOrders",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return openOrders
    }

    fun getLastOrdersRetrievedAt(): String {
        var lastDtString: String = ""

        try {
            db.beginTransaction()

            val cursor = db.rawQuery(SQL_GET_LAST_ORDERS_RETRIEVED_AT, null)

            cursor?.use {
                if (it.moveToFirst()) {
                    lastDtString = it.getString(0) ?: ""
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getLastOrdersRetrievedAt",
                t = ex))
        } catch (ex: IllegalStateException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getLastOrdersRetrievedAt",
                t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }

        return lastDtString
    }

    fun assumeLastOrderAsReference(): Int {
        var lastOrderID: Int = 0

        try {
            db.beginTransaction()

            val cursor = db.rawQuery(SQL_GET_MAX_ORDER_NUMBER, null)

            cursor?.use {
                if (it.moveToFirst()) {
                    lastOrderID = it.getInt(0)
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "assumeLastOrderAsReference",
                t = ex))
        } catch (ex: IllegalStateException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "assumeLastOrderAsReference",
                t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }

        return lastOrderID
    }
}
