package biz.deliverydudes.vendorapp.helpers.Network

import android.content.Context
import biz.deliverydudes.vendorapp.models.LogEntry
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.internal.http.HttpHeaders
import okio.Buffer
import okio.GzipSource
import org.joda.time.DateTime
import java.io.EOFException
import java.io.File
import java.io.IOException
import java.nio.charset.Charset

// This code is modified from okhttp3.logging.HttpLoggingInterceptor.
class ProdDBuggerInterceptor(private val ctx: Context) : Interceptor {
    private val UTF8 = Charset.forName("UTF-8")
    private val filenameToday = DateTime().toString("YYYY-MM-dd") + ".txt"

    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain!!.request()
        val response: Response

        try {
            response = chain.proceed(request)
        } catch (e: Exception) {
            throw e
        }

        val responseBody = response.body()
        val contentLength = responseBody!!.contentLength()
        val responseHeaders = response.headers()

        if (!HttpHeaders.hasBody(response)) {
            // logger.log("<-- END HTTP")
        } else if (bodyHasUnknownEncoding(response.headers())) {
            // logger.log("<-- END HTTP (encoded body omitted)")
        } else {
            val source = responseBody.source()
            source.request(java.lang.Long.MAX_VALUE) // Buffer the entire body.
            var buffer = source.buffer()

            var gzippedLength: Long? = null
            val contentEncoding = responseHeaders.get("Content-Encoding") ?: ""

            if ("gzip".equals(contentEncoding, ignoreCase = true)) {
                gzippedLength = buffer.size()
                var gzippedResponseBody: GzipSource? = null
                try {
                    gzippedResponseBody = GzipSource(buffer.clone())
                    buffer = Buffer()
                    buffer.writeAll(gzippedResponseBody)
                } finally {
                    gzippedResponseBody?.close()
                }
            }

            var charset: Charset? = UTF8
            val contentType = responseBody.contentType()
            if (contentType != null) {
                charset = contentType.charset(UTF8)
            }

            if (!isPlaintext(buffer)) {
                return response
            }

            if (contentLength != 0L) {
                if (response.header("content-type") == "application/json") {
                    // logger.log("")
                    // logger.log(buffer.clone().readString(charset!!))
                    logToFile(buffer.clone().readString(charset!!))
                }
            }
        }

        return response
    }

    private fun deleteOlderThanYesterday() {
        val filenameYesterday = DateTime().plusDays(-1).toString("YYYY-MM-dd") + ".txt"
        val filesInFolder = File(ctx.filesDir.absolutePath).listFiles()

        // delete any files that is not today's file or yesterday's file.
        for (f: File in filesInFolder) {
            if (f.name != filenameYesterday && f.name != filenameToday) {
                f.delete()
            }
        }
    }

    private fun logToFile(message: String) {
        if (message.isEmpty())
            return

        val fileToday = File(ctx.filesDir.absolutePath, filenameToday)
        if (!fileToday.exists()) {
            try {
                fileToday.createNewFile()
                deleteOlderThanYesterday()
            } catch (ex: IOException) {
                Log_LogEntry(LogEntry(context = ctx,
                    message = "There was an issue creating file $filenameToday.",
                    sourceClass = javaClass.name,
                    sourceMethod = "logToFile",
                    t = ex))
            }
        }

        try {
            val timeStamp = DateTime().toString()
            fileToday.appendText("\n$timeStamp \n$message")
        } catch (ex: IOException) {
            Log_LogEntry(LogEntry(context = ctx,
                message = "There was an issue adding text to ${fileToday.name}.",
                sourceClass = javaClass.name,
                sourceMethod = "logToFile",
                t = ex))
        }
    }

    private fun isPlaintext(buffer: Buffer): Boolean {
        try {
            val prefix = Buffer()
            val byteCount = if (buffer.size() < 64) buffer.size() else 64
            buffer.copyTo(prefix, 0, byteCount)
            for (i in 0..15) {
                if (prefix.exhausted()) {
                    break
                }
                val codePoint = prefix.readUtf8CodePoint()
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false
                }
            }
            return true
        } catch (e: EOFException) {
            return false // Truncated UTF-8 sequence.
        }
    }

    private fun bodyHasUnknownEncoding(headers: Headers): Boolean {
        val contentEncoding = headers.get("Content-Encoding")
        return (contentEncoding != null &&
            !contentEncoding.equals("identity", ignoreCase = true) &&
            !contentEncoding.equals("gzip", ignoreCase = true))
    }
}