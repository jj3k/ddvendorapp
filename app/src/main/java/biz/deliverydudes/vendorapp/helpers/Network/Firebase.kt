package biz.deliverydudes.vendorapp.helpers.Network

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics

// remember to update the play store (Google Play Services) for firebase to work.
class Firebase(context: Context, order_id: Int) {
    constructor(context: Context) : this(context, 0) { }

    val fba = FirebaseAnalytics.getInstance(context)

    companion object {
        val bundle = Bundle()
    }

    init {
        bundle.putString("order_id", order_id.toString())
    }

    // assign_time
    fun assign_time() {
        fba.logEvent("assign_time", bundle)
    }

    // change_order_time
    fun change_order_time() {
        fba.logEvent("change_order_time", bundle)
    }

    // ready_for_pickup
    fun ready_for_pickup() {
        fba.logEvent("ready_for_pickup", bundle)
    }

    // submit_feedback
    fun submit_feedback() {
        fba.logEvent("submit_feedback", bundle)
    }

    // update_order_time
    fun update_order_time() {
        fba.logEvent("update_order_time", bundle)
    }

    fun view_past_order() {
        fba.logEvent("view_past_order", bundle)
    }

    // view_order
    fun view_order() {
        fba.logEvent("view_order", bundle)
    }
}