package biz.deliverydudes.vendorapp.helpers.Network

import android.util.Log
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.models.LogEntry
import com.logentries.logger.AndroidLogger
import java.io.File

class Log_LogEntry(log: LogEntry) {
    private var logger: AndroidLogger? = null
    private val token: String = if (BuildConfig.DEBUG) "b7e4d1b7-b4fa-4ee8-99a0-3217450adea0" else "fb1d57cb-652d-4e4f-9ef0-9ded5047c98e"
    val local_log_fname = "LogentriesLogStorage.log"

    init {
        try {
            val path = File(log.context.filesDir.absolutePath, local_log_fname)

            // E/LogentriesAndroidLogger: Cannot load logs from the local storage:
            // /data/user/0/biz.deliverydudes.vendorapp/files/LogentriesLogStorage.log (No such file or directory)
            if (!path.exists()) {
                path.createNewFile()
            }

            logger = AndroidLogger.createInstance(log.context,
                    false,
                    true,
                    false,
                    null,
                    0,
                    token,
                    true)

            if (BuildConfig.DEBUG) {
                Log.i(javaClass.name, log.message)

                if (log.error_message.isNotBlank()) {
                    Log.e(javaClass.name, log.error_message)
                }
            }

            logger?.log(log.toString())
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                Log.e(javaClass.name, "There was an issue with LogEntries that could not be logged to LogEntries. ${e.message}")
            }
        }
    }
}
