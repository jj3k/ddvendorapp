package biz.deliverydudes.vendorapp.helpers.Network

import android.content.Context
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.models.ITB
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import biz.deliverydudes.vendorapp.models.User
import io.reactivex.Observable
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface APIService {
    @FormUrlEncoded
    @POST(url_base_login)
    fun login(
        @Field("login") email: String,
        @Field("password") password: String,
        @Field("android_id") android_id: String,
        @Field("app_version") app_version: String,
        @Field("os_version") os_version: String
    ): Observable<ResponseBody>

    @GET(url_base_logout)
    fun logout(): Observable<ResponseBody>

    @POST(url_base_forgot_password)
    fun forgotPassword(@Query("login", encoded = false) email: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST(url_base_ticket_program_feedback)
    fun sendFeedback(
        @Field("feedback_type") feedback_type: String,
        @Field("message") message: String,
        @Field("order_id") order_id: Int,
        @Field("app_version") app_version: String,
        @Field("os_version") os_version: String
    ): Observable<ResponseBody>

    @GET(url_base_order)
    fun getOrder(@Path("order_id") orderID: Int): Observable<Order>

    // since, updated_after and offset
    @GET(url_base_orders)
    fun getOrders(@Query("updated_after", encoded = true) updated_after: String): Observable<ArrayList<Order>>

    @GET(url_base_orders)
    fun getOrders(
        @Query("offset") offset: Int,
        @Query("since", encoded = true) since: String,
        @Query("limit") limit: Int
    ): Observable<ArrayList<Order>>

    @GET(url_base_orders)
    fun getOrders(
        @Query("since", encoded = true) since: String,
        @Query("limit") limit: Int,
        @Query("sort_by_id") sort_by_id: String
    ): Observable<ArrayList<Order>>

    @GET(url_base_ping)
    fun ping(): Observable<ResponseBody>

    @GET(url_base_account)
    fun getVendorAccount(): Observable<User>

    @FormUrlEncoded
    @PUT(url_base_order)
    fun setReadyAt(
        @Path("order_id") order_id: Int,
        @Field("ready_at", encoded = true) readyAt: String,
        @Field("duration_update") duration_update: String
    ): Observable<Order>

    @POST("http://rest.logentries.com/management/logs")
    fun log(@Body log: LogEntry): Observable<ResponseBody>

    @GET(url_get_all_itb_emails)
    fun getAllITBEmails(): Observable<ArrayList<ITB>>

    @POST(url_add_or_delete_itb_email)
    fun addITBinAPI(@Query("email", encoded = false) email: String): Observable<ITB>

    @DELETE(url_add_or_delete_itb_email)
    fun deleteITBinAPI(
        @Query("id") id: Int,
        @Query("email") email: String
    ): Observable<ResponseBody>

    // "Headers that need to be added to every request can be specified using an OkHttp interceptor." -- http://square.github.io/retrofit/
    companion object {
        val API_URL: String = "https://api" + (if (BuildConfig.DEBUG) "dev" else "") + ".deliverydudes.biz"

        const val url_base_login = "/login/vendor"
        const val url_base_logout = "/logout"
        const val url_base_forgot_password = "/forgot_password/vendor"
        const val url_base_ticket_program_feedback = "/vendor_app/feedback"
        const val url_base_order = "/vendor_app/orders/{order_id}"
        const val url_base_orders = "/v2/vendor_app/orders"
        const val url_base_ping = "/vendor_app/ping"
        const val url_base_account = "/vendor_app/account"
        const val url_add_or_delete_itb_email = "/vendor_app/in_the_biz"
        const val url_get_all_itb_emails = "/vendor_app/in_the_biz"

        fun createAPIService(context: Context): APIService {
            val httpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(GlobalInterceptor(context))
                .addInterceptor(ProdDBuggerInterceptor(context))

            if (BuildConfig.DEBUG) {
                val httpLoggingIC = HttpLoggingInterceptor()
                httpLoggingIC.level = HttpLoggingInterceptor.Level.BODY

                httpClient.addInterceptor(httpLoggingIC)
            }

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(DDGsonConverterFactory().getDDFactory())
                .client(httpClient.build())
                .baseUrl(API_URL)
                .build()

            return retrofit.create(APIService::class.java)
        }
    }
}