package biz.deliverydudes.vendorapp.helpers.Database

import android.content.Context
import android.database.sqlite.SQLiteException
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

class SuggestTime(private val context: Context) {
    companion object {
        const val SQL_CREATE_ORDERS_TIMESTAMPS = "CREATE TABLE IF NOT EXISTS orders_timestamps (order_id INTEGER, \n" +
            "received_on DATE, \n" +
            "assigned_time_on DATE, \n" +
            "ready_for_pickup_on DATE); \n" +
            "CREATE INDEX idx_assigned_times ON orders_timestamps(assigned_time_on); \n" +
            "CREATE INDEX idx_RFP_times ON orders_timestamps(ready_for_pickup_on); "

        // region TRIGGERS
        const val SQL_CREATE_ON_ORDER_RECEIVED = "CREATE TRIGGER IF NOT EXISTS onOrderReceived \n" +
            "AFTER insert on orders \n" +
            "WHEN UPPER(new.status) = 'PLACED' \n" +
            "begin \n" +
                "INSERT INTO orders_timestamps (order_id, received_on) \n" +
                "VALUES (new.id, strftime('%Y-%m-%dT%H:%M:%S', 'now')); \n" +
            "end;"

        // we want this to run ONLY when the order is assigned time.
        const val SQL_CREATE_ON_ORDER_UPDATED_ASSIGNED = "CREATE TRIGGER IF NOT EXISTS onOrderUpdated_Assigned \n" +
            "AFTER update on orders \n" +
            "WHEN UPPER(new.status) = 'ASSIGNED' \n" +
            "begin \n" +
                "UPDATE orders_timestamps SET assigned_time_on = strftime('%Y-%m-%dT%H:%M:%S', 'now') \n" +
                "WHERE orders_timestamps.order_id = new.id \n" +
                "AND orders_timestamps.ready_for_pickup_on is null; \n" +
            "end;"

        // we want this to run ONLY when the order is marked RFP
        const val SQL_CREATE_ON_ORDER_UPDATED_RFP = "CREATE TRIGGER IF NOT EXISTS onOrderUpdated_RFP \n" +
            "AFTER update on orders \n" +
            "WHEN (UPPER(new.status) = 'RFP') \n" +
            "begin \n" +
                "UPDATE orders_timestamps SET ready_for_pickup_on = strftime('%Y-%m-%dT%H:%M:%S', 'now') \n" + // , assigned_time = new.food_ready \n" +
                "WHERE order_id = new.id \n" +
                    "AND orders_timestamps.ready_for_pickup_on is null; \n" +
            "end;"

        // we want this to run ONLY when the order is unmarked RFP, by adding time
        const val SQL_CREATE_ON_ORDER_UPDATED_UNRFP = "CREATE TRIGGER IF NOT EXISTS onOrderUpdated_UNRFP \n" +
            "AFTER update on orders \n" +
            "WHEN (UPPER(new.status) = 'UNRFP') \n" +
            "begin \n" +
                "UPDATE orders_timestamps SET ready_for_pickup_on = NULL \n" +
                "WHERE order_id = new.id \n" +
                "AND not(orders_timestamps.ready_for_pickup_on is null); \n" +
            "end;"
        // endregion

        // region VIEWS
        const val SQL_VIEW_PRODUCTS_QUANTITIES_DURATION = "CREATE view IF NOT EXISTS vProducts_Quantities_Duration \n" +
            "AS \n" +
            "SELECT ci.quantity, o.id as order_id, \n" +
                "24 * 60 * 60 * (julianday(ifnull(ots.ready_for_pickup_on, o.pick_time)) - julianday(ifnull(ots.assigned_time_on, o.placed_on))) as order_duration \n" +
            "FROM vOrder_Quantities ci \n" +
            "INNER JOIN orders o on o.id = ci.id \n" +
            "LEFT JOIN orders_timestamps ots on o.id = ots.order_id \n" +
            "WHERE (UPPER(o.status) = 'PICKEDUP' OR UPPER(o.status) = 'DELIVERED') \n" +
                "AND pick_time <> '0001-01-01T00:00:00Z'; "

        const val SQL_VIEW_PROJ_BASED_ON_CART_SIZE = "CREATE view IF NOT EXISTS vProjection_Based_On_Cart_Size AS \n" +
            " SELECT quantity, AVG(order_duration) as avg_order_duration \n" +
            " FROM vProducts_Quantities_Duration \n" +
            " GROUP BY quantity;"

        const val SQL_VIEW_ORDER_QUANTITIES = "CREATE view IF NOT EXISTS vOrder_Quantities \n" +
            "AS \n" +
            "SELECT o.id, SUM(quantity) as quantity \n" +
            "FROM cart_items ci \n" +
            "INNER JOIN orders o on o.id = ci.order_id \n" +
            "WHERE (UPPER(o.status) = 'PICKEDUP' OR UPPER(o.status) = 'DELIVERED') \n" +
            "GROUP BY o.id;"
        // endregion

        const val SQL_DELETE_FROM_ORDERS_TIMESTAMPS = "DELETE FROM orders_timestamps"
        const val SQL_GET_PROJECTION = "SELECT avg_order_duration FROM vProjection_Based_On_Cart_Size WHERE quantity = "
        const val SQL_GET_ORDERS_TIMESTAMPS_RFP = "SELECT ready_for_pickup_on FROM orders_timestamps WHERE ready_for_pickup_on IS NOT NULL AND order_id = "
    }

    private val db = LocalDB.instance(context).writableDatabase

    fun getSuggestionInMins(numItems: Int): Int {
        // -5 to communicate no suggestion was made
        var nearest5 = -5.0

        try {
            db.beginTransaction()

            val cursor = db.rawQuery(SuggestTime.SQL_GET_PROJECTION + numItems.toString(), null)

            cursor?.use {
                if (it.moveToFirst()) {
                    nearest5 = it.getDouble(0) / 60

                    // you cannot suggest zero
                    nearest5 = max(nearest5, 5.0)
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getSuggestionInMins",
                t = ex))
        } finally {
            db.endTransaction()
        }

        // slider only goes to 60
        return min((nearest5.roundToInt() / 5) * 5, 60)
    }

    fun isRFP(o: Order): Boolean {
        var bReturn = false

        if (!db.isOpen)
            return false

        try {
            db.beginTransaction()

            val cursor = db.rawQuery(SuggestTime.SQL_GET_ORDERS_TIMESTAMPS_RFP + o.id.toString(), null)

            cursor?.use {
                bReturn = cursor.count > 0 || o.status == "RFP"
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "isRFP",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return bReturn
    }
}