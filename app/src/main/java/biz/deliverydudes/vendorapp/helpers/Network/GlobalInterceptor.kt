package biz.deliverydudes.vendorapp.helpers.Network

import android.content.Context
import android.net.ConnectivityManager
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.models.LogEntry
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

// http://codezlab.com/add-cookies-interceptor-using-retrofit-android/
/*  @ctx    Context     Accepts as input a wrapped context object. */
class GlobalInterceptor(private val ctx: Context) : Interceptor {
    private val mgr: ConnectivityManager = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val androidVersion = android.os.Build.VERSION.RELEASE
        val strUserAgent = "DDTrafficControl/Version=${BuildConfig.VERSION_NAME}/Platform=Android/PlatformVersion=$androidVersion"

        if (!isOnline()) {
            throw NoConnectionException(ctx)
        }

        val request = chain.request()
            .newBuilder()
            .addHeader("User-Agent", strUserAgent)

        val cookie = Common(ctx).getFromSharedPrefs(Common.cache.Cookie.code)
        if (cookie != "") {
            request.addHeader("Cookie", cookie)
        }

        val originalResponse = chain.proceed(request.build())

        if (!originalResponse.headers("Set-Cookie").isEmpty()) {
            for (header in originalResponse.headers("Set-Cookie")) {
                // save the new cookie
                Common(ctx).addToSharedPrefs(Common.cache.Cookie.code, header)
                break
            }
        }

        Log_LogEntry(
            LogEntry(
                context = ctx,
                http_request_base_url = chain.request().url().toString(),
                http_request_type = chain.request().method(),
                // retrofit only lets you read the response body once.
                http_response_body = "",
                http_response_code = originalResponse.code().toString(),
                message = originalResponse.message().toString(),
                params = Common.logEntryParams(chain.request().body())
            )
        )

        return originalResponse
    }

    private fun isOnline(): Boolean {
        val netInfoMob = mgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        val netInfoWifi = mgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        // SM-T350 (Tab A) has netInfoMob null
        if (netInfoMob?.isConnectedOrConnecting == true) {
            return true
        }

        if (netInfoWifi?.isConnectedOrConnecting == true) {
            return true
        }

        return false
    }
}

class NoConnectionException(private val ctx: Context) : IOException() {
    override fun getLocalizedMessage(): String {
        return ctx.getString(R.string.no_network_or_api_down)
    }
}