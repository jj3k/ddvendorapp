package biz.deliverydudes.vendorapp.helpers.Database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class LocalDB(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object { 
        /// TODO remember that the version in prod is 4
        const val DATABASE_VERSION = 4 
        const val DATABASE_NAME = "TrafficControl.db"

        private var instance: LocalDB? = null

        @Synchronized
        fun instance(context: Context): LocalDB {
            if (instance == null) {
                instance = LocalDB(context.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        // all 'create' statements are create if not exist statements (safe)
        db.execSQL(OrdersTable.SQL_CREATE_ORDERS_TABLE)
        db.execSQL(CartItemsTable.SQL_CREATE_CART_ITEMS)
        db.execSQL(UsersTable.SQL_CREATE_USERS_TABLE)

        // these two column extensions would only fail if they're already in there.
        try {
            db.execSQL(CartItemsTable.SQL_ADD_PRODUCT_ID)
        } catch (ex: SQLiteException) {
            // do nothing
        }

        try {
            db.execSQL(UsersTable.SQL_UPGRADE_ADD_ITB_CAP)
        } catch (ex: SQLiteException) {
            // do nothing
        }

        db.execSQL(SuggestTime.SQL_CREATE_ORDERS_TIMESTAMPS)
        db.execSQL(SuggestTime.SQL_CREATE_ON_ORDER_RECEIVED)
        db.execSQL(SuggestTime.SQL_CREATE_ON_ORDER_UPDATED_ASSIGNED)
        db.execSQL(SuggestTime.SQL_CREATE_ON_ORDER_UPDATED_RFP)
        db.execSQL(SuggestTime.SQL_CREATE_ON_ORDER_UPDATED_UNRFP)

        db.execSQL(SuggestTime.SQL_VIEW_PRODUCTS_QUANTITIES_DURATION)
        db.execSQL(SuggestTime.SQL_VIEW_PROJ_BASED_ON_CART_SIZE)
        db.execSQL(SuggestTime.SQL_VIEW_ORDER_QUANTITIES)

        db.execSQL(ITBTable.SQL_CREATE_ITB_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        onCreate(db!!)
    }

    fun logout() {
        this.writableDatabase.execSQL(CartItemsTable.SQL_DELETE_CART_ITEMS)
        this.writableDatabase.execSQL(OrdersTable.SQL_DELETE_ORDERS)
        this.writableDatabase.execSQL(UsersTable.SQL_DELETE_FROM_USERS)
        this.writableDatabase.execSQL(SuggestTime.SQL_DELETE_FROM_ORDERS_TIMESTAMPS)
        this.writableDatabase.execSQL(ITBTable.SQL_DELETE_FROM_ITB)

        // this.close()
    }
}