package biz.deliverydudes.vendorapp.helpers.Database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteException
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.User

class UsersTable(private val context: Context) {

    private val db = LocalDB.instance(context).writableDatabase

    // model data class
    companion object {
        const val TABLE_NAME = "users"
        const val ID = "id"
        const val BANK_ACCOUNT_ID = "bank_account_id"
        const val BILLING_ADDRESS_ID = "billing_address_id"
        const val BILLING_EMAIL = "billing_email"
        const val EMAIL = "email"
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val PHONE = "phone"
        const val USER_ROLE = "user_role"
        const val VENDOR_ID = "vendor_id"
        const val IN_THE_BIZ_CAP = "in_the_biz_cap"

        const val SQL_CREATE_USERS_TABLE = """CREATE TABLE IF NOT EXISTS users
            (
                id BIG INT PRIMARY KEY NOT NULL,
                bank_account_id BIG INT,
                billing_address_id BIG INT,
                billing_email TEXT,
                email TEXT,
                first_name TEXT,
                last_name TEXT,
                phone TEXT,
                user_role INTEGER,
                vendor_id BIG INT,
                in_the_biz_cap SMALLINT DEFAULT 15
            )"""

        const val SQL_SELECT_1USER = "SELECT id, bank_account_id, billing_address_id, billing_email, email, first_name, last_name, phone, user_role, vendor_id, in_the_biz_cap FROM users LIMIT 1"
        const val SQL_CHECK_USERS = "SELECT 1 FROM sqlite_master WHERE type = 'table' AND tbl_name = 'users' limit 1"
        const val SQL_DELETE_FROM_USERS = "DELETE FROM users"
        const val SQL_DROP_USERS = "DROP TABLE IF EXISTS users"
        const val SQL_CHECK_EXISTS_USERS = "SELECT 1 FROM sqlite_master WHERE type = 'table' AND tbl_name = 'users' limit 1"
        const val SQL_UPGRADE_ADD_ITB_CAP = "ALTER TABLE users ADD in_the_biz_cap SMALLINT DEFAULT 15"
    }

    fun setAccountInfo(user: User) {
        db.beginTransaction()
        // check if user table exists
        val cursor: Cursor = db.rawQuery(SQL_CHECK_USERS, null)

        try {
            cursor.use {
                it.moveToFirst()

                val bNoUsersTable = it.isAfterLast

                // if !exist, create it, if exist, delete record.
                db.execSQL(if (bNoUsersTable) SQL_CREATE_USERS_TABLE else SQL_DELETE_FROM_USERS)

                val cv: ContentValues = ContentValues()
                cv.put(ID, user.id)
                cv.put(BILLING_ADDRESS_ID, user.billing_address_id)
                cv.put(EMAIL, user.email)
                cv.put(FIRST_NAME, user.first_name)
                cv.put(LAST_NAME, user.last_name)
                cv.put(PHONE, user.phone)
                cv.put(USER_ROLE, user.user_role)
                cv.put(VENDOR_ID, user.vendor_id)
                cv.put(IN_THE_BIZ_CAP, user.in_the_biz_cap)

                db.insert(TABLE_NAME, null, cv)
            }

            db.setTransactionSuccessful()
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "setAccountInfo",
                t = ex))
        } finally {
            db.endTransaction()
        }
    }

    fun getAccountInfo(): User {
        val userReturn = User()

        try {
            db.beginTransaction()
            val cursor = db.rawQuery(SQL_SELECT_1USER, null)

            cursor?.use {
                if (it.moveToFirst()) {
                    userReturn.apply {
                        id = it.getInt(it.getColumnIndex(ID))
                        billing_address_id = it.getInt(it.getColumnIndex(BILLING_ADDRESS_ID))
                        billing_email = it.getString(it.getColumnIndex(BILLING_EMAIL)) ?: ""
                        email = it.getString(it.getColumnIndex(EMAIL))
                        first_name = it.getString(it.getColumnIndex(FIRST_NAME))
                        last_name = it.getString(it.getColumnIndex(LAST_NAME))
                        phone = it.getString(it.getColumnIndex(PHONE))
                        user_role = it.getInt(it.getColumnIndex(USER_ROLE))
                        vendor_id = it.getInt(it.getColumnIndex(VENDOR_ID))
                        in_the_biz_cap = it.getInt(it.getColumnIndex(IN_THE_BIZ_CAP))
                    }
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getAccountInfo",
                t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }

        return userReturn
    }
}