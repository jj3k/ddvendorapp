package biz.deliverydudes.vendorapp.helpers.Network

import android.app.Activity
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.google.firebase.messaging.RemoteMessage
import com.pusher.android.PusherAndroid
import com.pusher.android.notifications.PushNotificationRegistration
import com.pusher.android.notifications.fcm.FCMPushNotificationReceivedListener
import com.pusher.android.notifications.tokens.PushNotificationRegistrationListener
import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils

class PusherClient(val activity: Activity, private val callback: (() -> Unit)) : FCMPushNotificationReceivedListener {
    private val ctx = VendorContextWrapper(activity.applicationContext)
    private val vendor_id = Common(ctx).getIntFromSharedPrefs(Common.cache.VendorID.code)
    private val _vendorChannel = getChannel("vid-$vendor_id")
    private val apiBridge = APIBridge(ctx)
    private val dbBridge = DBBridge(ctx)

    private fun logEntries(le: LogEntry): Runnable {
        return Runnable() {
            Log_LogEntry(le)
        }
    }

    companion object globalPusher {
        var bIsSubscribed = false
        var vendorChannel: String = ""
            set(value) { field = value } // 'redundant'

        val key = if (BuildConfig.DEBUG) "515a376057fb7a2b4ebd" else "4eadc06e94b2a9cf89e7"
        // lemongrass is vid-1 => "a2d1e7554c0ad6f4c95801304244c76b"
        // mellowm is vid-72 => "8d0e5163aeada70f24b12ff5e13ca032"
        // zireetest is vid-26 => "5b06e2f5e3cf548decad9cc0939e58eb"
        // puckettTest is vid-2817 => "4d0a6ea5608ad44e6a3a28c3d3054334"
        private val pusher = PusherAndroid(key)
        val nativePusher: PushNotificationRegistration = pusher.nativePusher()

        fun signout() {
            nativePusher.unsubscribe(vendorChannel)
            pusher.disconnect()
            bIsSubscribed = false
        }
    }

    fun subscribe() {
        vendorChannel = _vendorChannel

        if (!bIsSubscribed) {
            nativePusher.subscribe(vendorChannel)
        }

        nativePusher.registerFCM(ctx, object : PushNotificationRegistrationListener {
            override fun onSuccessfulRegistration() {
                val leSuccess = LogEntry(context = ctx,
                    message = "Successfully subscribed to pusher channel $vendorChannel.")

                activity.runOnUiThread(logEntries(leSuccess))
                bIsSubscribed = true
            }

            override fun onFailedRegistration(statusCode: Int, response: String) {
                val leFail = LogEntry(context = ctx,
                    http_response_body = response,
                    http_response_code = "statusCode $statusCode",
                    error_module = "PusherClient.kt",
                    error_submodule = "subscribe()",
                    message = "Failed to subscribe to pusher channel $vendorChannel.")

                activity.runOnUiThread(logEntries(leFail))
            }
        })

        nativePusher.setFCMListener {
            onMessageReceived(it)
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // example body : Updated Order: <ORDERNUMBER>
        // example sound: "order_notification"
        val body = remoteMessage.notification?.body
        val sound = remoteMessage.notification?.sound

        if (body.isNullOrEmpty()) return
        if (body?.contains("UPDATED VENDOR", true) ?: false) return

        val orderID = body!!.replace(Regex("\\D+"), "").toInt()
        val order = dbBridge.getOrder(nOrderID = orderID)

        val logMessageReceived = LogEntry(context = ctx,
            http_request_base_url = "(Pusher notification)",
            http_response_body = body,
            sourceClass = "PusherClient.kt",
            sourceMethod = "onMessageReceived",
            message = "The app received a pusher notification re: $orderID. Body: $body. Sound: $sound.")

        activity.runOnUiThread(logEntries(logMessageReceived))

        when (sound) {
            "order_notification" -> {
                apiBridge.getAPIOrder(orderID, {
                    callback()
                })
            }
            "cancelled_notification" -> {
                // if it's not empty, there's an order in the db to cancel. If it's already cancelled, pass.
                if (!order.isEmpty() &&
                       Order.Status.from(order.status) != Order.Status.Cancelled) {
                    Common(ctx).soundCancellation()

                    dbBridge.cancelOrder(orderID, callback)
                }
            }
            "picked_notification" -> {
                if (Order.Status.from(order.status) != Order.Status.PickedUp)
                    Common(ctx).soundPicking()

                dbBridge.updateOrderDBAsPicked(order, callback)
            }
        }
    }

    private fun getChannel(vid: String): String {
        // https://stackoverflow.com/questions/9126567/method-not-found-using-digestutils-in-android
        val charArrayChannel = Hex.encodeHex(DigestUtils.md5(vid))
        val channelName = charArrayChannel.joinToString(separator = "") { it.toString() }

        return channelName
    }
}