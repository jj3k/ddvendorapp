package biz.deliverydudes.vendorapp.helpers

import android.content.Context
import android.content.ContextWrapper

class VendorContextWrapper(context: Context) : ContextWrapper(context)