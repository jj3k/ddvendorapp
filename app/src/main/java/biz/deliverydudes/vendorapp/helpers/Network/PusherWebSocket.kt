package biz.deliverydudes.vendorapp.helpers.Network

import android.app.Activity
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.channel.ChannelEventListener
import com.pusher.client.connection.ConnectionEventListener
import com.pusher.client.connection.ConnectionStateChange
import org.apache.commons.codec.binary.Hex
import org.apache.commons.codec.digest.DigestUtils

// This is the non-FCM websocket version (com.pusher:pusher-java-client).
// Also, this listerner listens ONLY for vendor_updated events. NEVER order_udpated!
class PusherWebSocket(val activity: Activity, val cb: (() -> Unit)): ChannelEventListener, ConnectionEventListener {
    private val ctx = VendorContextWrapper(activity.applicationContext)
    private val apiBridge = APIBridge(ctx)
    private val vendor_id = Common(ctx).getIntFromSharedPrefs(Common.cache.VendorID.code)
    private val _vendorChannel = getChannel("vid-$vendor_id")

    private fun logEntries(le: LogEntry): Runnable {
        return Runnable() {
            Log_LogEntry(le)
        }
    }

    init {
        if (!bIsSubscribed) {
            pusher.connect()

            // just a precaution
            try {
                pusher.subscribe(_vendorChannel, this, "vendor_updated")
            } catch (ex: IllegalArgumentException)
            {
                // you should never come here.
                // you got here by resubscribing when you
                // were already subscribed.
                val leFail = LogEntry(context = ctx,
                        http_response_body = ex.message.toString(),
                        error_module = "PusherWebSocket.kt",
                        error_submodule = "init",
                        message = "The application attempted to resubscribe to pusher via web socket.")

                activity.runOnUiThread(logEntries(leFail))
            }
        }

        apiBridge.onGetVendorInfoSucceeded = {
            Common(ctx).onGetVendorInfoSucceeded(it, { activity.runOnUiThread(cb) })
        }
    }

    companion object globalPusherWS {
        private var options: PusherOptions = PusherOptions().setEncrypted(true)
        private val key = PusherClient.key

        var bIsSubscribed = false
        var vendorChannel: String = ""
            set(value) { field = value } // 'redundant'

        var pusher: Pusher = Pusher(key, options)

        fun signout() {
            pusher.unsubscribe(vendorChannel)
            pusher.disconnect()
            bIsSubscribed = false
        }
    }

    override fun onEvent(channelName: String?, eventName: String?, data: String?) {
        val logMessageReceived = LogEntry(context = ctx,
                http_request_base_url = "(Pusher web socket notification)",
                http_response_body = data ?: "An empty data string was received.",
                sourceClass = "PusherWebSocket.kt",
                sourceMethod = "onEvent",
                message = "The app received a pusher notification via web socket. Channel: $channelName. Event: $eventName. Data: $data")

        activity.runOnUiThread(logEntries(logMessageReceived))

        apiBridge.getVendorInfo()
    }

    override fun onSubscriptionSucceeded(channelName: String?) {
        val leSuccess = LogEntry(context = ctx,
                message = "Successfully subscribed to websocket pusher channel $channelName.")

        activity.runOnUiThread(logEntries(leSuccess))
        bIsSubscribed = true
        vendorChannel = _vendorChannel
    }

    override fun onConnectionStateChange(change: ConnectionStateChange?) {
        val leChange = LogEntry(context = ctx,
            sourceClass = "PusherWebSocket.kt",
            sourceMethod = "onConnectionStateChange",
            message = "The subscription object to websocket pusher channel $_vendorChannel changed state from ${change?.previousState} to ${change?.currentState}.")

        activity.runOnUiThread(logEntries(leChange))
    }

    override fun onError(message: String?, code: String?, e: Exception?) {
        val exceptionMessage = (e ?: Exception("An unknown exception was encountered.")).message.toString()
        val leFail = LogEntry(context = ctx,
                http_response_body = exceptionMessage,
                http_response_code = code ?: "No code received.",
                error_module = "PusherWebSocket.kt",
                error_submodule = "onError()",
                message = message ?: "A null message was received by onError().")

        activity.runOnUiThread(logEntries(leFail))
    }

    private fun getChannel(vid: String): String {
        // https://stackoverflow.com/questions/9126567/method-not-found-using-digestutils-in-android
        val charArrayChannel = Hex.encodeHex(DigestUtils.md5(vid))
        val channelName = charArrayChannel.joinToString(separator = "") { it.toString() }

        return channelName
    }
}