package biz.deliverydudes.vendorapp.helpers.Database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.ITB
import biz.deliverydudes.vendorapp.models.LogEntry

class ITBTable(private val context: Context) {
    private val db = LocalDB.instance(context).writableDatabase

    // model data class
    companion object {
        const val TABLE_NAME = "ITB"
        const val ID = "id"
        const val VENDOR_ID = "vendor_id"
        const val FIRST_NAME = "first_name"
        const val LAST_NAME = "last_name"
        const val EMAIL_ADDRESS = "email_address"

        const val SQL_CREATE_ITB_TABLE = """CREATE TABLE IF NOT EXISTS `ITB` (
            `id` INTEGER PRIMARY KEY,
            `vendor_id` INTEGER,
            `first_name` TEXT,
            `last_name` TEXT,
            `email_address` TEXT )"""

        const val SQL_SELECT_ALL_ITBs = "SELECT id, vendor_id, first_name, last_name, email_address FROM ITB ORDER BY first_name COLLATE NOCASE ASC"
        const val SQL_DELETE_FROM_ITB = "DELETE FROM ITB"
        const val SQL_DELETE1_FROM_ITB = "DELETE FROM ITB WHERE email_address = "
    }

    fun getAllITBs(): ArrayList<ITB> {
        // note, all ITBs are deleted onPingSucceeded. See "hard synchronization"
        val itbReturn = ArrayList<ITB>()

        try {
            db.beginTransaction()
            val cursor = db.rawQuery(SQL_SELECT_ALL_ITBs, null)

            cursor?.use {
                while (it.moveToNext()) {
                    val itb = ITB()
                    itb.apply {
                        id = it.getInt(it.getColumnIndex(ID))
                        vendor_id = it.getInt(it.getColumnIndex(VENDOR_ID))
                        first_name = it.getString(it.getColumnIndex(FIRST_NAME))
                        last_name = it.getString(it.getColumnIndex(LAST_NAME))
                        email_address = it.getString(it.getColumnIndex(EMAIL_ADDRESS))
                    }

                    itbReturn.add(itb)
                }
            }

            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "getAllITBs",
                t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }

        return itbReturn
    }

    fun insertITB(itb: ITB): Boolean {
        // insert
        // Create a new map of values, where column names are the keys
        val itb_values = ContentValues()

        // no Update, just Create, Read, Delete
        itb_values.apply {
            put(ID, itb.id)
            put(VENDOR_ID, itb.vendor_id)
            put(FIRST_NAME, itb.first_name)
            put(LAST_NAME, itb.last_name)
            put(EMAIL_ADDRESS, itb.email_address)
        }

        try {
            db.beginTransaction()
            val newRowId = db.insert(TABLE_NAME, null, itb_values)
            db.setTransactionSuccessful()
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "insertITB",
                t = ex))
        } finally {
            db.endTransaction()
        }

        return true
    }

    fun insertITBs(itbs: ArrayList<ITB>): ArrayList<ITB> {
        val allITBsInDB = getAllITBs()

        for (itb: ITB in itbs) {
            val itbDB = allITBsInDB.filter {
                it.email_address == itb.email_address
            }
            val bNotInDB = itbDB.isEmpty()

            if (bNotInDB)
                insertITB(itb)
        }

        return itbs
    }

    fun deleteAllITBsInDB() {
        try {
            db.beginTransaction()

            db.execSQL(SQL_DELETE_FROM_ITB)
            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                    error_module = javaClass.name,
                    error_submodule = "deleteAllITBsInDB",
                    t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }
    }

    fun deleteITBinDB(email: String) {
        try {
            db.beginTransaction()

            db.execSQL(SQL_DELETE1_FROM_ITB + "'" + email + "'")
            db.setTransactionSuccessful()
        } catch (ex: SQLiteException) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "deleteITBinDB",
                t = ex))
        } finally {
            // "please stop crashing."
            if (db.isOpen)
                db.endTransaction()
        }
    }
}