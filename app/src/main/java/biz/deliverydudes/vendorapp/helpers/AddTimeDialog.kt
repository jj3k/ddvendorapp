package biz.deliverydudes.vendorapp.helpers

import android.app.Activity
import android.content.ContextWrapper
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import biz.deliverydudes.vendorapp.APIBridge
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.DBBridge
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Network.Firebase
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import biz.deliverydudes.vendorapp.recyclerViews.OrderActivity
import com.amplitude.api.Amplitude
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import retrofit2.HttpException

class AddTimeDialog {
    private var alertDialog: AlertDialog? = null
    private lateinit var wrappedContext: ContextWrapper

    fun showDialog(t: Activity, appOrderId: Int, order: Order) {
        wrappedContext = VendorContextWrapper(t.applicationContext)
        val apiBridge = APIBridge(wrappedContext)
        val dbBridge = DBBridge(wrappedContext)

        Amplitude.getInstance().logEvent("Selects Add Time")

        val dialogView = t.layoutInflater.inflate(R.layout.change_pickup_time, null)

        val builder = AlertDialog.Builder(t)
        builder.let {
            it.setCancelable(true)
            it.setView(dialogView)
            alertDialog = it.create()
        }
        alertDialog?.show()

        val txtNewPickupTime = dialogView.findViewById<TextView>(R.id.txtNewPickupTime)
        val txtNum = dialogView.findViewById<TextView>(R.id.txtNumMinutes)
        var numMinutesSelected = txtNum.text.replace(Regex("\\D+"), "").toInt()
        val btnUpdate = dialogView.findViewById<Button>(R.id.btnUpdate)
        val bRFP = dbBridge.isRFP(order)

        // update button defaults to disabled. enabled = numMinutes != 0
        btnUpdate.isEnabled = false
        // TODO use regular styles (disabled, got focus)
        btnUpdate.setBackgroundColor(ContextCompat.getColor(wrappedContext, R.color.ddDisabledButton))

        // if the food ready time has been eclipsed, add to now
        val originalReadyAt = DateTime(order.placed_on).plusMinutes(order.foodReady)
        val timeRemainingMillis = originalReadyAt.millis - System.currentTimeMillis()
        val bExpired = timeRemainingMillis <= 0
        val timeRemainingMinutes = (timeRemainingMillis / 60000).toInt()

        // on init of dialog, show pickup time (this will change with button clicks '-' or '+')
        var readyAt = LocalDateTime()
                .plusMinutes(if (bExpired) 0 else timeRemainingMinutes)
                .plusMinutes(numMinutesSelected)

        var readyAtFriendly = readyAt.toString("hh:mm a")
        txtNewPickupTime.text = "New pickup time: $readyAtFriendly"

        val plus5 = dialogView.findViewById<Button>(R.id.btnPlus5)
        plus5.setOnClickListener {
            numMinutesSelected = (numMinutesSelected + 5).rem(65)
            txtNum.setText("$numMinutesSelected minutes")

            readyAt = LocalDateTime()
                    .plusMinutes(if (bExpired) 0 else timeRemainingMinutes)
                    .plusMinutes(numMinutesSelected)

            readyAtFriendly = readyAt.toString("hh:mm a")

            txtNewPickupTime.text = "New pickup time: $readyAtFriendly"

            btnUpdate.isEnabled = numMinutesSelected != 0
            // TODO use regular styles (disabled, got focus)
            btnUpdate.setBackgroundColor(ContextCompat.getColor(wrappedContext,
                    if (numMinutesSelected == 0) R.color.ddDisabledButton else R.color.primary))
        }

        val minus5 = dialogView.findViewById<Button>(R.id.btnMinus5)
        minus5.setOnClickListener {
            val oldNumMinutes = numMinutesSelected
            numMinutesSelected = (numMinutesSelected - 5).rem(65)
            txtNum.setText("$numMinutesSelected minutes")

            readyAt = LocalDateTime()
                    .plusMinutes(if (bExpired) 0 else timeRemainingMinutes)
                    .plusMinutes(numMinutesSelected)

            readyAtFriendly = readyAt.toString("hh:mm a")

            txtNewPickupTime.text = "New pickup time: $readyAtFriendly"

            if (readyAt.isBefore(LocalDateTime())) {
                val toastText = wrappedContext.resources.getString(R.string.pickup_time_must_be_in_future)
                Toast.makeText(wrappedContext, toastText, Toast.LENGTH_SHORT).show()

                // "rollback"
                numMinutesSelected = oldNumMinutes.rem(65)
                txtNum.setText("$numMinutesSelected minutes")

                readyAt = LocalDateTime()
                        .plusMinutes(if (bExpired) 0 else timeRemainingMinutes)
                        .plusMinutes(numMinutesSelected)

                readyAtFriendly = readyAt.toString("hh:mm a")

                txtNewPickupTime.text = "New pickup time: $readyAtFriendly"
            }

            btnUpdate.isEnabled = numMinutesSelected != 0
            // TODO use regular styles (disabled, got focus)
            btnUpdate.setBackgroundColor(ContextCompat.getColor(wrappedContext,
                    if (numMinutesSelected == 0) R.color.ddDisabledButton else R.color.primary))
        }

        val lp = dialogView.layoutParams
        lp.height = 500

        dialogView.layoutParams = lp

        btnUpdate.setOnClickListener({
            Amplitude.getInstance().logEvent("Updates Time", Common.JSONForAmplitude(hashMapOf("minutes" to numMinutesSelected)))

            alertDialog?.dismiss()

            Firebase(wrappedContext, order.id).change_order_time()

            val timeRemainingMinutes5 = (timeRemainingMinutes / 5) * 5

            // update the order in the cloud with timestamp = now(utc) + assigned time
            val newReadyAt = DateTime(DateTimeZone.UTC)
                    .plusMinutes(if (bExpired) 0 else timeRemainingMinutes5)
                    .plusMinutes(numMinutesSelected)

            apiBridge.setReadyAt(order.id, newReadyAt, false)

            apiBridge.onOrderUpdated = {
                order.foodReady = it.foodReady
                order.placed_on = it.placed_on
                order.modified_on = it.modified_on

                if (bRFP) {
                    // this is for onOrderUpdated_UNRFP.
                    order.status = "UNRFP"
                }

                dbBridge.updateOrderDB(order)

                apiBridge.getAPIOrder(order.id, {
                    val refreshOrders = Intent("refresh_orders")
                    t.sendBroadcast(refreshOrders)
                })

                // called from TabOrders Fragment and from the Order Activity
                if (t is OrderActivity) {
                    val resultIntent = Intent()
                    resultIntent.putExtra("position", appOrderId)
                    resultIntent.putExtra("AssignedTimeInMinutes", 0)

                    t.setResult(Activity.RESULT_OK, resultIntent)
                    t.finish()
                } else {
                    // make in-line (TabOrders) button clickable
                    // (was disabled to prevent mult clicks)
                    val btnAddTime = t.findViewById<Button>(R.id.btnAddTime)
                    btnAddTime.isEnabled = true
                }
            }

            apiBridge.onOrderUpdateFailed = {
                onOrderUpdateFailed(t, it)
            }
        })
    }

    fun onOrderUpdateFailed(t: Activity, it: Throwable) {
        var code = 0
        wrappedContext = VendorContextWrapper(t.applicationContext)
        alertDialog?.dismiss()
        if (BuildConfig.DEBUG) Log.e(javaClass.name, "Failed to assign minutes to the order. Error message ${it.message}")

        try {
            code = (it as HttpException).code()
        } catch (ex: ClassCastException) {
            // no connection exception cannot be cast to an HttpException
            Log_LogEntry(LogEntry(context = wrappedContext,
                    error_module = javaClass.name,
                    error_submodule = "onOrderUpdatedFailed",
                    message = ex.message.toString(),
                    t = it
            ))
        } finally {
            // the order was withdrawn just after they went to assign time
            when (code) {
                422 -> {
                    val toastText = t.application.resources.getString(R.string.order_status_changed)
                    Toast.makeText(t, toastText, Toast.LENGTH_LONG).show()
                }
                0 -> {}
                else -> {
                    Log_LogEntry(LogEntry(context = wrappedContext,
                        error_module = javaClass.name,
                        error_submodule = "onOrderUpdatedFailed",
                        message = "Http code $code: An unknown error was encountered.",
                        t = it))
                }
            }
        }
    }
}