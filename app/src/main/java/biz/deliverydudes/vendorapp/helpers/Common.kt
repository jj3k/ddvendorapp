package biz.deliverydudes.vendorapp.helpers

import android.content.Context
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.preference.PreferenceManager
import android.text.TextUtils
import android.util.Log
import biz.deliverydudes.vendorapp.BuildConfig
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Database.UsersTable
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import biz.deliverydudes.vendorapp.models.User
import biz.deliverydudes.vendorapp.recyclerViews.TabOrders
import com.amplitude.api.Amplitude
import com.crashlytics.android.Crashlytics
import okhttp3.FormBody
import okhttp3.RequestBody
import org.joda.time.DateTime
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.concurrent.TimeUnit

class Common(private val context: Context) {
    val sp: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    enum class cache(val code: String) {
        AndroidID("android_id"),
        Email("email"),
        InTheBizCap("in_the_biz_cap"),
        Cookie("cookie"),
        VendorID("vendor_id")
    }

    fun addToSharedPrefs(key: String, value: String) {
        sp
            .edit()
            .apply {
                putString(key, value)
                apply()
            }
    }

    fun addToSharedPrefs(key: String, value: Int) {
        sp
            .edit()
            .apply {
                putInt(key, value)
                apply()
            }
    }

    fun getFromSharedPrefs(key: String, defaultValue: String = ""): String {
        return sp.getString(key, defaultValue).toString()
    }

    fun getIntFromSharedPrefs(key: String, defaultValue: Int = 0): Int {
        return sp.getInt(key, defaultValue)
    }

    fun clearSharedPrefs() {
        addToSharedPrefs(cache.AndroidID.code, "")
        addToSharedPrefs(cache.Email.code, "")
        addToSharedPrefs(cache.Cookie.code, "")
        addToSharedPrefs(cache.VendorID.code, 0)
    }

    fun loadRawResource(id: Int): String {
        val rawResource = context.resources.openRawResource(id)
        val br = BufferedReader(InputStreamReader(rawResource))
        var finalString = ""

        try {
            br.use {
                r -> r.lineSequence().forEach {
                    finalString += it + "\n"
                }
            }
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = context,
                error_module = javaClass.name,
                error_submodule = "Common.loadRawResource",
                message = "There was an error opening a raw resource.",
                t = ex))
        } finally {
            return finalString
        }
    }

    /* fun getDimensions() {
        val displayMetrics = context.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
    } */

    companion object {
        fun timeRemaining(order: Order): Long {
            val readyAt = DateTime(order.placed_on).plusMillis(order.foodReady * 60000)
            val timeRemaining = readyAt.millis - System.currentTimeMillis()

            if (order.status != Order.Status.Placed.code && order.status != Order.Status.Assigned.code)
                return 0

            /* Log.i("runPs", readyAt.toString())
            Log.i("runPs", "System.currentTimeMillis()::" + System.currentTimeMillis().toString())
            Log.i("runPs", "timeRemaining.toString()::" + timeRemaining.toString()) */

            return timeRemaining
        }

        // https://stackoverflow.com/questions/9027317/how-to-convert-milliseconds-to-hhmmss-format/24327049
        fun mmSS(inbound: Long): String {
            val nHour = TimeUnit.MILLISECONDS.toHours(inbound)
            val nMinutes = TimeUnit.MILLISECONDS.toMinutes(inbound) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(inbound))
            val nSeconds = TimeUnit.MILLISECONDS.toSeconds(inbound) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(inbound))

            return if (nHour > 0) String.format("%02d:%02d:%02d", nHour, nMinutes, nSeconds)
                    else String.format("%02d:%02d", nMinutes, nSeconds)
        }

        /*  Returns true or false based on whether the email address is valid.
            @inbound    email address   the email address to validate  */
        fun isValidEmail(inbound: String): Boolean {
            if (TextUtils.isEmpty(inbound))
                return false

            val bValid = android.util.Patterns.EMAIL_ADDRESS.matcher(inbound).matches()
            return bValid
        }

        // This attempts to smooth out the key value pairs being
        // passed by retrofit in the Request Body (sometimes a Form Body).
        // If it's null, there's nothing to do here.
        fun logEntryParams(rb: RequestBody?): String {
            if (rb == null) return ""

            try {
                val fb = rb as FormBody
                var returnFlatKVPairs = ""

                for (i in 1..fb.size()) {
                    val key = fb.encodedName(i - 1).trim()
                    returnFlatKVPairs += key + "=" + (if (key.toLowerCase() == "password") "*" else fb.encodedValue(i - 1)) + " "
                }

                return returnFlatKVPairs
            } catch (ex: ClassCastException) {
                // okhttp3.MultipartBody cannot be cast to okhttp3.FormBody
                return ""
            } catch (ex: Exception) {
                return ""
            }
        }

        fun JSONForAmplitude(nOrderID: Int): JSONObject {
            val json = JSONObject()
            json.put("Order ID", nOrderID)

            return json
        }

        fun JSONForAmplitude(kv: HashMap<String, Int>): JSONObject {
            val json = JSONObject()

            for (kvPair in kv) {
                json.put(kvPair.key, kvPair.value)
            }

            return json
        }
    }

    // the media player objects are
    // companion objects on the TabOrders page
    fun soundNotification() {
        if (TabOrders.mpOrder?.isPlaying == true) {
            return
        }

        TabOrders.mpOrder = MediaPlayer.create(context, R.raw.order_notification)
        TabOrders.mpOrder?.isLooping = true
        TabOrders.mpOrder?.start()
    }

    fun soundCancellation() {
        stfu()

        TabOrders.mpCancelled = MediaPlayer.create(context, R.raw.cancelled_notification)
        TabOrders.mpCancelled?.isLooping = false
        TabOrders.mpCancelled?.start()
    }

    fun soundPicking() {
        stfu()

        TabOrders.mpPicked = MediaPlayer.create(context, R.raw.picked_notification)
        TabOrders.mpPicked?.isLooping = false
        TabOrders.mpPicked?.start()
    }

    fun stfu() {
        // the stfu coming from the orders queue when there
        // are no unassigned orders is trampling on the picked and
        // cancelled sounds.
        Thread.sleep(600)
        TabOrders.mpOrder?.release()
        TabOrders.mpCancelled?.release()
        TabOrders.mpPicked?.release()

        TabOrders.mpOrder = null
        TabOrders.mpCancelled = null
        TabOrders.mpPicked = null
    }

    fun onGetVendorInfoSucceeded(u: User, cb: () -> Unit) {
        val cookie = Common(context).getFromSharedPrefs(Common.cache.Cookie.code)
        if (BuildConfig.DEBUG) Log.i(javaClass.name, "Logged in. Got cookie: $cookie.")

        val strRestaurantName = "${u.first_name} ${u.last_name}"

        addToSharedPrefs(Common.cache.Email.code, u.email)
        addToSharedPrefs(Common.cache.InTheBizCap.code, u.in_the_biz_cap)
        addToSharedPrefs(Common.cache.VendorID.code, u.vendor_id)

        UsersTable(context).setAccountInfo(u)

        Log_LogEntry(LogEntry(context = context,
                message = "Successfully saved vendor info to Shared Preferences.",
                sourceClass = javaClass.name,
                sourceMethod = "apiBridge.onSetVendorInfoSucceeded"))

        if (!BuildConfig.DEBUG)
            logCrashlyticsUser(u.vendor_id.toString(), u.email, strRestaurantName)

        val userProps = JSONObject()
                .put("email", u.email)
                .put("restaurant", strRestaurantName)
                .put("phone_number", u.phone)
                .put("territory", u.territory_id)
        Amplitude.getInstance().setUserProperties(userProps)

        cb()
    }

    private fun logCrashlyticsUser(identifier: String, email: String, username: String) {
        Crashlytics.setUserIdentifier(identifier)
        Crashlytics.setUserEmail(email)
        Crashlytics.setUserName(username)
    }
}