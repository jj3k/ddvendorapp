package biz.deliverydudes.vendorapp.helpers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import biz.deliverydudes.vendorapp.About
import biz.deliverydudes.vendorapp.FeedbackAboutQuestion
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.SignoutActivity

open class DDAppCompatActivity(private val activityName: String) : AppCompatActivity() {

    enum class activityCode(val code: String) {
        About("About"),
        FeedbackAboutQuestion("FeedbackAboutQuestion"),
        FeedbackActivity("FeedbackActivity"),
        FreeDeliveries("FreeDeliveries"),
        OrderActivity("OrderActivity"),
        Tabs("Tabs")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_main, menu)

        when (activityName) {
            activityCode.About.code -> {
                hideOption(menu, R.id.action_about)
            }
            activityCode.FeedbackActivity.code -> {
                hideOption(menu, R.id.action_feedback)
            }
            activityCode.FeedbackAboutQuestion.code -> {
                hideOption(menu, R.id.action_feedback)
            }
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        when (id) {
            R.id.action_feedback -> {
                val fb = Intent(this, FeedbackAboutQuestion::class.java)
                startActivity(fb)

                return true
            }
            R.id.action_signout -> {
                val so = Intent(this, SignoutActivity::class.java)
                // so.putExtra("key", value)
                startActivity(so)

                return true
            }
            R.id.action_about -> {
                val so = Intent(this, About::class.java)
                // so.putExtra("key", value)
                startActivity(so)

                return true
            }
            R.id.empty -> { }
            else -> {
                finish()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun hideOption(menu: Menu, id: Int) {
        menu.findItem(id).setVisible(false)
    }
}