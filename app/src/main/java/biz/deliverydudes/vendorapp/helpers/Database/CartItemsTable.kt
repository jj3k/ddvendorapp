package biz.deliverydudes.vendorapp.helpers.Database

import android.database.sqlite.SQLiteDatabase

class CartItemsTable(val db: SQLiteDatabase) {
    companion object {
        const val TABLE_NAME = "cart_items"
        const val ID = "id"
        const val ADDITIONAL_INSTRUCTIONS = "additional_instructions"
        const val CATEGORY_NAME = "category_name"
        const val PRODUCT_DESCRIPTION = "product_description"
        const val PRODUCT_ID = "product_id"
        const val PRODUCT_NAME = "product_name"
        const val PRODUCT_PRICE = "product_price"
        const val QUANTITY = "quantity"
        const val ORDER_ID = "order_id"

        const val SQL_CREATE_CART_ITEMS = "CREATE TABLE IF NOT EXISTS cart_items( " +
                "id BIG INT PRIMARY KEY NOT NULL, " +
                "additional_instructions TEXT, " +
                "product_id BIG INT, " +
                "category_name TEXT, " +
                "product_description TEXT, " +
                "product_name TEXT, " +
                "product_price TEXT, " +
                "quantity INTEGER, " +
                "order_id BIG INT )"

        const val SQL_ADD_PRODUCT_ID = "ALTER TABLE cart_items ADD product_id BIG INT"

        const val SQL_DELETE_CART_ITEMS = "DELETE FROM cart_items"

        const val SQL_DROP_CART_ITEMS = "DROP TABLE IF EXISTS cart_items"

        const val SQL_CHECK_EXISTS_CART_ITEMS = "SELECT 1 FROM sqlite_master WHERE type = 'table' AND tbl_name = 'cart_items' limit 1"
    }
}
