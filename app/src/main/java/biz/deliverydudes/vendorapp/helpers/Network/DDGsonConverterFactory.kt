package biz.deliverydudes.vendorapp.helpers.Network

import biz.deliverydudes.vendorapp.models.CartItem
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type
import kotlin.math.roundToInt

class DDGsonConverterFactory {
    private val customOrderDeserializer = JsonDeserializer<Order> { json: JsonElement, _: Type, _: JsonDeserializationContext ->
        val jsonObject = json.asJsonObject
        var new_order = Order(
            id = 0,
            foodReady = 0,
            status = "",
            items = arrayListOf(CartItem()),
            pickedup_on = "",
            placed_on = "",
            tax = 0.0,
            modified_on = ""
        )

        try {
            jsonObject.apply {
                new_order = Order(
                    id = get("id").asInt,
                    foodReady = get("food_ready").asInt,
                    status = get("status").asString,
                    items = arrayListOf(CartItem()),
                    pickedup_on = get("pickedup_on").asString,
                    placed_on = get("placed_on").asString,
                    tax = get("tax").asDouble,
                    modified_on = get("modified_on").asString
                )
            }
        } catch (ex: Exception) {
            LogEntry.log(javaClass.name, "DDGsonConverterFactory.customOrderDeserializer", ex.message.toString())
        }

        // total = subtotal + tax
        val subtotal = jsonObject.get("sub_total").asDouble
        // totals are stored as integers $27.99 -> 2799 (prevents rounding errs)
        val total = (100 * (subtotal + new_order.tax)).roundToInt().toString()

        new_order.total = total

        // now for the cart items
        val cart_items = jsonObject.get("cart_items").asJsonObject
        val num_cart_items = cart_items.size()
        val output_cart_items: ArrayList<CartItem> = ArrayList<CartItem>()

        for (i in 0..num_cart_items - 1) {
            var this_cart_item: CartItem = CartItem()

            try {
                val current_item = cart_items.get("$i").asJsonObject

                current_item.apply {
                    val cid = get("cart_item_id").asInt
                    val catName = get("category_name").asString
                    val prodID: Int = get("product_id").asInt
                    val prodName = get("product_name").asString
                    val addlInstructions = get("additional_instructions").asString
                    val productPrice = get("product_price").asString
                    val qty = get("quantity").asInt

                    this_cart_item = CartItem(
                        cart_item_id = cid,
                        categoryName = catName,
                        productId = prodID,
                        productName = prodName,
                        additionalInstructions = addlInstructions,
                        productPrice = productPrice,
                        quantity = qty
                    )
                }
            } catch (ex: Exception) {
                LogEntry.log(javaClass.name, "DDGsonConverterFactory.customOrderDeserializer", ex.message.toString())
            }

            output_cart_items.add(this_cart_item)
        }

        new_order.items = output_cart_items

        new_order
    }

    private val gsonBuilder = GsonBuilder()

    fun getDDFactory(): GsonConverterFactory {
        gsonBuilder.registerTypeAdapter(Order::class.java, customOrderDeserializer)
        return GsonConverterFactory.create(gsonBuilder.create())
    }
}

