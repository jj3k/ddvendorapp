package biz.deliverydudes.vendorapp

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.Network.NoConnectionException
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.activity_signin.*

class SigninActivity : AppCompatActivity() {
    private val wrappedContext = VendorContextWrapper(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        readDeviceID()

        val pInfo = this.packageManager.getPackageInfo(packageName, 0)
        val version = pInfo.versionName + (if (BuildConfig.DEBUG) " D" else " P")
        val apiBridge = APIBridge(wrappedContext)
        val strBadUnameOrPwd = resources.getString(R.string.bad_uname_or_pwd)

        setContentView(R.layout.activity_signin)

        versionInfo.text = version
        textReleaseNotes.text = Html.fromHtml(Common(wrappedContext).loadRawResource(R.raw.release_notes))

        apiBridge.onLoginSucceeded = {
            Amplitude.getInstance().logEvent("Selects Login")
            apiBridge.getVendorInfo()

            frameReleaseNotes.visibility = View.VISIBLE
            btnDoneReading.visibility = View.VISIBLE
            landingPage.setPadding(0, 0, 0, 0)

            // get all ITBs emails from the API. They were deleted from the DB on logout.
            apiBridge.getAllITBEmailsAPI()

            // wvDemo?.loadUrl("file:///android_asset/Bear.mp4")
            // wvDemo?.loadUrl("file:///android_asset/street.gif")
            // wvDemo?.settings?.mediaPlaybackRequiresUserGesture = false
        }

        apiBridge.onLoginFailed = {
            frameReleaseNotes.visibility = View.GONE

            if (it is NoConnectionException)
                Toast.makeText(wrappedContext, it.localizedMessage, Toast.LENGTH_LONG).show()
            else
                Log_LogEntry(LogEntry(context = wrappedContext,
                    message = "There was an issue signing in; perhaps bad creds, perhaps more serious.",
                    sourceClass = javaClass.name,
                    sourceMethod = "apiBridge.onLoginFailed",
                    t = it))

            Toast.makeText(wrappedContext, strBadUnameOrPwd, Toast.LENGTH_LONG).show()
        }

        apiBridge.onGetVendorInfoSucceeded = {
            Common(wrappedContext).onGetVendorInfoSucceeded(it, {})
        }

        apiBridge.onForgotPasswordSucceeded = {
            Amplitude.getInstance().logEvent("Selects Forgot Password")

            val code = it.raw().code()
            val strLinkSent = this.applicationContext.resources.getString(R.string.reset_password_link_sent)

            Log_LogEntry(LogEntry(context = wrappedContext,
                message = "onForgotPasswordSucceeded passed with code $code",
                sourceClass = javaClass.name,
                sourceMethod = "apiBridge.onForgotPasswordSucceeded"))

            Toast.makeText(wrappedContext, strLinkSent, Toast.LENGTH_LONG).show()
        }

        btnLogin.setOnClickListener { it ->
            // pesky whitespace
            username.setText(username.text.toString().trim())

            val username = username.text.toString()
            val pw = password.text.toString()
            val strLogin = getString(R.string.login)
            val bIsLoggingIn = (it as Button).text == strLogin

            if (Common.isValidEmail(username)) {
                if (bIsLoggingIn) {
                    val android_id = readDeviceID()
                    val app_version = BuildConfig.VERSION_NAME
                    val os_version = android.os.Build.VERSION.RELEASE

                    apiBridge.login(username, pw, android_id, os_version, app_version)
                } else
                    apiBridge.forgotPassword(username)
            } else {
                Toast.makeText(wrappedContext, strBadUnameOrPwd, Toast.LENGTH_LONG).show()
            }
        }

        forgot_password.setOnClickListener {
            Amplitude.getInstance().logEvent("Visits Forgot Password")

            it.context.resources.run {
                val strLogin = getString(R.string.login)
                val strForgotPassword = getString(R.string.forgot_password)
                val strCancel = getString(R.string.cancel)
                val strSendResetLink = getString(R.string.send_reset_link)

                password.visibility = if (password.visibility == View.INVISIBLE) View.VISIBLE else View.INVISIBLE
                btnLogin.setText(if (btnLogin.text == strLogin) strSendResetLink else strLogin)
                forgot_password.setText(if (forgot_password.text == strForgotPassword) strCancel else strForgotPassword)
            }
        }

        btnDoneReading.setOnClickListener {
            Amplitude.getInstance().logEvent("Done Reading Release Notes")

            // pusher uses vid-${user.vendor_id}
            val orders = Intent(wrappedContext, Tabs::class.java)
            startActivity(orders)

            finish()
        }
    }

    // @SuppressLint("MissingPermission")
    private fun readDeviceID(): String {
        val cr = this.getContentResolver()

        // reads the device id into shared preferences
        val android_id = Settings.Secure.getString(cr, Settings.Secure.ANDROID_ID)
        if (BuildConfig.DEBUG) Log.i(javaClass.name, "The Android ID on the device is $android_id")
        Common(wrappedContext).addToSharedPrefs(Common.cache.AndroidID.code, android_id)

        return android_id
    }
}