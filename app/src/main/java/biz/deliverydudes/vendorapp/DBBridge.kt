package biz.deliverydudes.vendorapp

import android.content.Context
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Database.ITBTable
import biz.deliverydudes.vendorapp.helpers.Database.LocalDB
import biz.deliverydudes.vendorapp.helpers.Database.OrdersTable
import biz.deliverydudes.vendorapp.helpers.Database.SuggestTime
import biz.deliverydudes.vendorapp.helpers.Network.APIService
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.models.ITB
import biz.deliverydudes.vendorapp.models.LogEntry
import biz.deliverydudes.vendorapp.models.Order
import com.amplitude.api.Amplitude

/*
    This layer is above above the API layer.
    All application calls to the API come here.
 */

class DBBridge(private val ctx: Context) {
    val database = LocalDB.instance(ctx)
    val ordersTable = OrdersTable(ctx)
    private val suggestTimesTable = SuggestTime(ctx)
    val itbTable = ITBTable(ctx)

    fun getOrder(nOrderID: Int): Order {
        return ordersTable.getOrder(nOrderID)
    }

    fun getOrderAt(nOffset: Int): Order {
        // get order by offset
        return ordersTable.getNthOrder(nOffset, Order.Mode.Current, "")
    }

    fun getHistoryOrderAt(nOffset: Int, orderBy: String): Order {
        return ordersTable.getNthOrder(nOffset, Order.Mode.History, orderBy)
    }

    fun assumeLastOrderAsReference(): Int {
        return ordersTable.assumeLastOrderAsReference()
    }

    fun getOpenOrders(): ArrayList<Order> {
        // get the open orders from the db
        return ordersTable.getOpenOrders()
    }

    // the count of open orders is passed as the offset
    fun getOpenOrderCount(): Int {
        return ordersTable.getOrderCount()
    }

    fun autoPickOrders12HrsOld(): Boolean {
        // orders that are over 12 hours old are autocancelled
        return ordersTable.autoCancel12HrOldOrders()
    }

    fun getHistoricalOrders(orderBy: String): ArrayList<Order> {
        return ordersTable.getHistoricalOrders(orderBy)
    }

    fun getOrderHistoryCount(): Int {
        return ordersTable.getOrderHistoryCount()
    }

    fun updateOrderDB(order: Order) {
        try {
            ordersTable.updateOrder(order)
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = ctx,
                    http_request_base_url = APIService.API_URL + APIService.url_base_order,
                    error_module = javaClass.name,
                    error_submodule = "updateOrderDB",
                    message = "An error was encountered trying to update order ${order.id} in the SQLite DB",
                    t = ex))
        }
    }

    fun updateOrderDBAsPicked(nOrderID: Int, cb: () -> Unit) {
        updateOrderDBAsPicked(getOrder(nOrderID), cb)
    }

    fun updateOrderDBAsPicked(o: Order, cb: () -> Unit) {
        try {
            ordersTable.updateOrderAsPicked(o.id)

            Amplitude.getInstance().logEvent("Order Picked Up", Common.JSONForAmplitude(o.id))
            Amplitude.getInstance().logRevenueV2(o.getRevenueObject())
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = ctx,
                    http_request_base_url = APIService.API_URL + APIService.url_base_order,
                    error_module = javaClass.name,
                    error_submodule = "updateOrderDBAsPicked",
                    message = "An error was encountered trying to update order ${o.id} (mark it as picked) in the SQLite DB.",
                    t = ex))
        } finally {
            cb()
        }
    }

    fun saveOrderToDB(order: Order) {
        saveOrdersToDB(arrayListOf(order), {})
    }

    fun saveOrdersToDB(orders: ArrayList<Order>, refreshUI: (() -> Unit)) {
        try {
            ordersTable.saveOrders(orders)

            refreshUI()
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = ctx,
                    http_request_base_url = APIService.API_URL + APIService.url_base_order,
                    error_module = javaClass.name,
                    error_submodule = "saveOrdersToDB",
                    message = "An error was encountered saving orders to the SQLite DB.",
                    t = ex))
        }
    }

    fun cancelOrder(nOrderID: Int, cb: () -> Unit) {
        try {
            ordersTable.cancelOrderById(nOrderID)
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = ctx,
                    http_request_base_url = APIService.API_URL + APIService.url_base_order,
                    error_module = javaClass.name,
                    error_submodule = "cancelOrder",
                    message = "An error was encountered cancelling an order in the SQLite DB.",
                    t = ex))
        } finally {
            cb()
        }
    }

    fun getSuggestionInMins(nCount: Int): Int {
        return suggestTimesTable.getSuggestionInMins(nCount)
    }

    fun isRFP(o: Order): Boolean {
        return suggestTimesTable.isRFP(o)
    }

    fun deleteAllITBsInDB() {
        itbTable.deleteAllITBsInDB()
    }

    fun getAllITBEmailsDB(): ArrayList<ITB> {
        return itbTable.getAllITBs()
    }
}