package biz.deliverydudes.vendorapp

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.ITB
import kotlin.math.min

interface ITBTrashButtonClickListener {
    fun onButtonClicked(position: Int): Unit
}

class ITBAdapter(
    private val ocl: ITBTrashButtonClickListener,
    val activity: Activity
) : BaseAdapter() {
    var data: List<ITB>? = null
    private var inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val ctx = VendorContextWrapper(this.activity.applicationContext)
    private var nITBCount = 0

    class ViewHolder {
        lateinit var textEmail: TextView
        lateinit var textName: TextView
        lateinit var image: ImageView
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var vi: View? = convertView
        val holder: ViewHolder = ViewHolder()

        convertView ?: run {
            vi = inflater.inflate(R.layout.itb_list_row, null)
        }

        holder.textEmail = vi?.findViewById(R.id.textEmail) as TextView
        holder.textName = vi?.findViewById(R.id.textName) as TextView
        holder.image = vi?.findViewById(R.id.image) as ImageView

        data?.let {
            if (!data!!.isEmpty()) {
                val thisITB = data!!.get(position)

                holder.textEmail.text = thisITB.email_address
                holder.textName.text = thisITB.first_name
                holder.image.setOnClickListener {
                    ocl.onButtonClicked(position)
                }
            }
        }

        return vi!!
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        val nITBEmailCap = Common(ctx).getIntFromSharedPrefs(Common.cache.InTheBizCap.code)
        nITBCount = data!!.size
        return min(nITBCount, nITBEmailCap)
    }
}
