package biz.deliverydudes.vendorapp

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.Network.NoConnectionException
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import com.amplitude.api.Amplitude
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import retrofit2.HttpException

class StartupActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Amplitude.getInstance()
                .initialize(this, if (BuildConfig.DEBUG) "e7bc98be3fa2a8e678cc4e3a75993c9d" else "dcad1a0de47c2c163f293b5b4c5943bb")
                .enableForegroundTracking(application)

        val wrappedContext = VendorContextWrapper(this)
        val apiBridge = APIBridge(wrappedContext)
        val dbBridge = DBBridge(wrappedContext)
        val email = Common(wrappedContext).getFromSharedPrefs(Common.cache.Email.code)
        val signIn = Intent(wrappedContext, SigninActivity::class.java)

        setContentView(R.layout.activity_splash)

        if (!BuildConfig.DEBUG)
            Fabric.with(wrappedContext, Crashlytics())

        if (email.isNotBlank())
            apiBridge.ping()
        else
            startActivity(signIn)

        apiBridge.onPingSucceeded = {
            apiBridge.getVendorInfo()

            // hard synchronization.
            dbBridge.deleteAllITBsInDB()

            // get all ITBs emails from the API
            apiBridge.getAllITBEmailsAPI()
        }

        apiBridge.onPingFailed = {
            apiBridge.onAPIFailure(it)

            if (it is NoConnectionException)
            // the toast was in onAPIFailure
            else {
                val code = (it as HttpException).code()

                Log_LogEntry(LogEntry(context = wrappedContext,
                        error_module = javaClass.name,
                        error_submodule = "onPingFailed",
                        message = "API responded with http code $code while attempting to ping.",
                        t = it
                ))
            }

            startActivity(signIn)
            finish()
        }

        // Mirrored in SigninActivity.kt.
        apiBridge.onGetVendorInfoSucceeded = {
            Common(wrappedContext).onGetVendorInfoSucceeded(it, {})

            // / TODO
            // / when ITB is completely rolled out, move this back to "onPingSucceeded"
            // / Right now the in_the_biz_cap has to be saved before the tab layout is
            // / rendered, otherwise, it won't have the value (nITBEmailCap) to set the boolean
            // / flag (bITBEnabled) to true.
            val orders = Intent(wrappedContext, Tabs::class.java)
            startActivity(orders)
            finish()
        }
    }
}
