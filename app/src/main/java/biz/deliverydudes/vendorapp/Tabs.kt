package biz.deliverydudes.vendorapp

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.DDAppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Network.PusherWebSocket
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.recyclerViews.TabHistory
import biz.deliverydudes.vendorapp.recyclerViews.TabOrders
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.activity_tabs.*

enum class tab(val idx: Int) {
    TabOrders(0),
    TabHistory(1),
    TabITB(2)
}

class Tabs : DDAppCompatActivity(DDAppCompatActivity.activityCode.Tabs.code) {
    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private val wrappedContext = VendorContextWrapper(this)
    private var nITBEmailCap = 0
    private var bITBEnabled = false
    lateinit var apiBridge: APIBridge
    lateinit var dbBridge: DBBridge

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        apiBridge = APIBridge(wrappedContext)
        nITBEmailCap = Common(wrappedContext).getIntFromSharedPrefs(Common.cache.InTheBizCap.code)
        bITBEnabled = nITBEmailCap != 0

        setContentView(R.layout.activity_tabs)

        val app_title = resources.getString(R.string.app_name)
        toolbar.title = app_title

        setSupportActionBar(toolbar)
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter
        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        configureTabLayout()

        PusherWebSocket(this, { drawTabs() })
        drawTabs()
    }

    // CB is the callback from the getVendorInfo
    private fun drawTabs() {
        dbBridge = DBBridge(wrappedContext)

        val nITBEmailCap = Common(wrappedContext).getIntFromSharedPrefs(Common.cache.InTheBizCap.code)
        val bTabWasShowing = mSectionsPagerAdapter?.count == 3
        bITBEnabled = nITBEmailCap != 0

        if (bITBEnabled) {
            if (!bTabWasShowing) {
                // add ITB to tabs
                val tabFreeDeliveries = tabs
                        .newTab()
                        .setText("Free Deliveries")
                tabs.addTab(tabFreeDeliveries, tab.TabITB.idx)
                // ... and to adapter
                mSectionsPagerAdapter?.addITB()
            }
        } else {
            if (bTabWasShowing) {
                // clear the table
                dbBridge.deleteAllITBsInDB()

                // remove ITB from tabs
                tabs.removeTabAt(tab.TabITB.idx)
                // ... and from adapter
                mSectionsPagerAdapter?.removeITB()
            }
        }

        // ... rebind and notify, but only if something changed.
        val bTabIsShowing = mSectionsPagerAdapter?.count == 3
        if (bTabIsShowing != bTabWasShowing) {
            container.adapter = mSectionsPagerAdapter
            mSectionsPagerAdapter?.notifyDataSetChanged()
        }
    }

    private fun configureTabLayout() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter
        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                container.currentItem = tab.position
                // 0 orders
                // 1 history
                // 2 free deliveries
                if (BuildConfig.DEBUG) Log.i(localClassName, "${tab.position} selected.")

                var amplitudeEvent = "Selects Orders"
                when (container.currentItem) {
                    1 -> {
                        amplitudeEvent = "Selects History"
                    }
                    2 -> {
                        amplitudeEvent = "Selects 'Free Deliveries'"
                    }
                }

                Amplitude.getInstance().logEvent(amplitudeEvent)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) { }

            override fun onTabReselected(tab: TabLayout.Tab) { }
        })
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * TabOrders of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private val tabsList = arrayListOf(TabOrders(), TabHistory(), TabITB())

        override fun getItem(position: Int): Fragment {
            return tabsList[position]
        }

        fun removeITB() {
            tabsList.removeAt(tab.TabITB.idx)
        }

        fun addITB() {
            tabsList.add(tab.TabITB.idx, TabITB())
        }

        override fun getCount(): Int {
            return tabsList.size
        }
    }
}
