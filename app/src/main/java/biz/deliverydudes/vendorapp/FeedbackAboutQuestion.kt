package biz.deliverydudes.vendorapp

import android.content.Intent
import android.os.Bundle
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.DDAppCompatActivity
import biz.deliverydudes.vendorapp.helpers.Network.Log_LogEntry
import biz.deliverydudes.vendorapp.helpers.VendorContextWrapper
import biz.deliverydudes.vendorapp.models.LogEntry
import com.amplitude.api.Amplitude
import kotlinx.android.synthetic.main.activity_feedback_about.*
import java.text.MessageFormat

/*
    Two pseudo buttons, one -- feedback is about the app (pseudoBtnApp)
    Two -- feedback is about the order (pseudoBtnOrder)
 */
class FeedbackAboutQuestion : DDAppCompatActivity(DDAppCompatActivity.activityCode.FeedbackAboutQuestion.code) {
    private val wrappedContext = VendorContextWrapper(this)
    private var ddOrderId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Amplitude.getInstance().logEvent("Selects Send Feedback")

        setContentView(R.layout.activity_feedback_about)

        // set the title then make the toolbar the action bar
        val toolbar_title = findViewById(R.id.toolbar) as android.support.v7.widget.Toolbar
        setSupportActionBar(toolbar_title)
        getSupportActionBar()?.setHomeButtonEnabled(true)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)

        try {
            // do something with $ddOrderId in the string/feedback_about_order
            if (intent.hasExtra("OrderID")) ddOrderId = intent.extras["OrderID"] as Int

            if (ddOrderId > 0) {
                // if you don't put a toString on ddOrderId, it comes with a comma, eg. 777,777
                val fbAboutOrder = MessageFormat.format(getString(R.string.feedback_about_order_num), ddOrderId.toString())
                orderBody.text = fbAboutOrder
            }
        } catch (ex: Exception) {
            Log_LogEntry(LogEntry(context = wrappedContext,
                error_module = javaClass.name,
                error_submodule = "onCreate",
                message = "An unexpected error!: " + ex.message,
                t = ex))
        }

        // add 'button' listeners
        pseudoBtnApp.setOnClickListener {
            Amplitude.getInstance().logEvent("Selects the App")

            val fb = Intent(this, FeedbackActivity::class.java)
            val bundle: Bundle = Bundle()
            bundle.putString("mode", "app")
            bundle.putInt("OrderID", ddOrderId)

            fb.putExtras(bundle)
            startActivityForResult(fb, 0)
        }

        pseudoBtnOrder.setOnClickListener {
            Amplitude.getInstance().logEvent("Selects the Order", Common.JSONForAmplitude(ddOrderId))

            val fb = Intent(this, FeedbackActivity::class.java)
            val bundle: Bundle = Bundle()
            bundle.putString("mode", "order")
            bundle.putInt("OrderID", ddOrderId)

            fb.putExtras(bundle)
            startActivity(fb)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        finish()
    }
}
