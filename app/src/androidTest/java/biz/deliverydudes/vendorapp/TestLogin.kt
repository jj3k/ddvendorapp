package biz.deliverydudes.vendorapp

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.agoda.kakao.KButton
import com.agoda.kakao.KEditText
import com.agoda.kakao.Screen
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

open class MainActivityScreen : Screen<MainActivityScreen>() {
    val username = KEditText { withId(R.id.username) }
    val password = KEditText { withId(R.id.password) }
    val loginButton = KButton { withId(R.id.btnLogin) }
}

@RunWith(AndroidJUnit4::class)
class TestLogin {
    // @get:Rule val activityTestRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Rule
    @JvmField
    val rule = ActivityTestRule(MockMainActivity::class.java, false, false)

    val screen = MainActivityScreen()

    /*
    @Test
    fun ableToClickLoginButton() {
        val appContext = InstrumentationRegistry.getTargetContext()

        onView(withId(R.id.username)).check(matches(withText("zireetest3@deliverydudes.com")))
        onView(withId(R.id.password)).check(matches(withText("runTheTest123456")))
    }

    // 0. check that some views are on screen.
    @Test
    fun loginButtonVisible() {
        // espresso helper
        // btnLogin.checkIsVisible()

        // normal espresso
        Espresso.onView(ViewMatchers.withId(R.id.btnLogin))
                .check(ViewAssertions.matches(
                        ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))


          android:id="@+id/Welcome"
          android:id="@+id/subTitle"
          android:id="@+id/username"
          android:id="@+id/password"
          android:id="@+id/forgot_password"
          android:id="@+id/btnLogin"
    }
    */

    @Test
    fun testOne() {
        screen {
            username {
                isVisible()
            }
            password {
                isVisible()
            }
            loginButton {
                isClickable()
                click()
            }
        }
    }
}
