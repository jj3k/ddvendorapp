package biz.deliverydudes.vendorapp
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.agoda.kakao.KButton
import com.agoda.kakao.Screen
import com.loopj.android.http.AsyncHttpClient.log
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

open class TestLogin_MAS : Screen<TestLogin_MAS>() {
    val loginButton = KButton { withId(R.id.btnLogin) }
}

// click the login button on the main screen
// email, pwd already populated, login should
// just go to the next activity
@RunWith(AndroidJUnit4::class)
class TestLogin_Mockito {
    lateinit var rb: RustBridge
    val screen = TestLogin_MAS()

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java, false, false)

    fun setUp() {
        MockitoAnnotations.initMocks(this)
        rb = mock(RustBridge::class.java)
    }

    @Test
    fun testLoginAndShowOrders() {

        `when`(rb.login("zireetest@deliverydudes.com", "runTheTest123456")).then {
            log.i("TestLogin_Mockito", "success")
        }

        screen {
            loginButton {
                click()
            }
        }
    }
}
