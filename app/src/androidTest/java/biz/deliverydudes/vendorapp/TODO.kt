

/*


Individual test cases
  1. When username and password fields have valid values, login button is enabled
  2. When forgot password is clicked, password field is gone, 'log in' button becomes 'send email', and is enabled when email has a valid value.
  3. When login is clicked, account email, current orders (or 'no orders' screen) is visible.
  4. When login is clicked, two tabs are visible, 'orders' and 'history.'
  5. For current orders: If an order has been assigned time, that time should be counting down in a visible pseudo button. If an
  order has not been assigned time, that order should show a button 'assign time.'

  From the orders screen
  1. When the menu is clicked, three items are visible: 1. send feedback, 2. sign out, 3. about traffic control.

  From the orders detail screen
  1. When an individual order is clicked, the order number is the page title, the amount is part of that title, the menu is visible (three dots),
    the slider is visible, defaulted to 15 minutes, and the assign time is visible (right now, it's below the fold).
  2. The cart items and quantities should be visible.
  3. A back button should be visible in the form of an arrow pointing left, at the top left of screen.
  4. If the order has not been assigned time, the 'assign time' button should be visible.
  5. If the order has been assigned time, a 'change time' button should be visible in the upper right.

  Change time modal
  1. If the 'change time' button is clicked, a modal window should be visible. Add and subtract buttons should be visible. "New pickup time" should
  show the current pickup time, as well as the new pickup time, if time were added or subtracted. 'Update time' should change the pseudo button
  time remaining on the current orders screen, to reflect the changed pickup time (unless the new time is in the past).

  From the history tab
  1. Historical orders should show in table layout.

  From the history order details screen
  1. When an individual historical order is clicked, everything is the same as the 'orders detail section' above, except that 'change time' and
  'assign time' should never show.



 */