package biz.deliverydudes.vendorapp.mock

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import biz.deliverydudes.vendorapp.R
import biz.deliverydudes.vendorapp.helpers.Common
import biz.deliverydudes.vendorapp.helpers.VendorException
import kotlinx.android.synthetic.main.activity_main.*

class MockMainActivity : AppCompatActivity() {

    private fun login(email: String, password: String) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener {
            // / TODO these inline throws aren't working, the exception is coming from hitting the api w null creds (400)
            val username = findViewById(R.id.username) as? EditText ?: throw VendorException(this, "need a username")
            val password = findViewById(R.id.password) as? EditText ?: throw VendorException(this, "need a password")

            val email = username.text.toString()
            val pw = password.text.toString()

            if (Common.isValidEmail(email))
                login(email, pw)
        }
    }
}
