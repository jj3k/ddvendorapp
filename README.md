The vendor app was rewritten from scratch in Kotlin in 2018. The code is checked-in as-is and unedited. No copyrights were added or removed. 

Self-documenting code really only works when the coder already knows the exceptions to the rules.

There are only a handful of things to watch out for, in future releases.

Log Entries. The AndroidLogger from Log Entries is designed to only run in the UI thread. This
means that when it's running in Pusher or in the Network Layer, it can have InvalidStateException(s).
Thanks to both Pusher and Retrofit, these so far have been inconsequential. In the future, we
might simply adopt the Java Log Entries object.

The database. Like any other SQLite database, it sure doesn't like being read from when it's closed.
At current writing, this is the only known cause of application crashes. In the future, I think we
should implement [Anko Sqlite](https://github.com/Kotlin/anko/wiki/Anko-SQLite).

The recycler view manifested an issue in production that it never had in development. The user
experience for this issue is either not reported or the user experiences no issues. But what the
logs report is an IndexOutOfBoundsException. It is trying to render an Nth element of an array of
size N - 1. This only happens when removing an order from the queue due to it being picked or
cancelled, never when the order is added. None of the solutions I found online worked.

Samsung is always breaking run-as. To view the database on the device:

1) adb backup -f ~/Desktop/va.db biz.deliverydudes.vendorapp
2) dd if=va.db bs=1 skip=24 | /usr/bin/python2.7 -c "import zlib,sys;sys.stdout.write(zlib.decompress(sys.stdin.read()))" > va.db.tar
3) move va.db.tar to a new folder then: tar xvf va.db.tar

Kotlin stock linter is not good; use detekt.

```git clone https://github.com/arturbosch/detekt
cd detekt
./gradlew build shadowJar
(where ~/software/detekt is the cloned path)
java -jar
~/software/detekt/detekt-cli/build/libs/detekt-cli-1.0.0-RC10-all.jar
```

NOTES:

1) app is only landscape, but android still seems to need all portrait layouts.
Before release, copy all layouts into the portrait folder.

2) Always clean before release builds.

3) Confirm Development and Release builds by installing and logging in. Check
for 2.x.y P for Production or D for Development on the login screen.

4) Even though there may be only one dev at a time working on this project, it's
better to not dev in development branch, but to merge from PRs. Makes it easier
to see what code is going into production, and to catch potential errors.


Release 2.2.5 notes:

Some odd behaviors can be expected if ITB caps are decreased without deleting
users in the tablet. Suppose, for instance, that our vendor's in-the-biz cap is
set at five. The cloud has five users; the tablet has five users.

An ops personnel decreases the number to four. The cloud has five and the tablet
has five, but due to the dynamic nature expected upon increase of ITB caps in
the tablet -- the tablet is showing four users.

Which one is not showing? The tablet orders alphabetically by first name.
Suppose that's Zachary. If the vendor ever adds Zachary again, the tablet will
say "You have reached your maximum of N accounts. Also, Zachary is already a
member."

This is because Zachary *is* already a member, only he is not showing on the
tablet.

It was considered to show all members in the tablet's database
(which come from the API), but it became too technically complicated to tell the
tablet to load ITB caps dynamically, but consider when that cap is shrinking vs.
expanding.

It's a moot discussion, in the sense that ITB caps are really supposed to be increasing, as
that is the point of the program. However, if it has to happen, the vendor
tablet user should reduce their number to the new cap size first. Then, when OPS
changes it in the cloud, everything should fall into place smoothly. This
includes going to zero, which shuts off the "Free Deliveries" tab in the app
(takes about 3 minutes).

For best results, use the console or the tablet to match the ITB cap before
setting it, or don't decrease it ever.
# DeliveryDudesVendorApp
